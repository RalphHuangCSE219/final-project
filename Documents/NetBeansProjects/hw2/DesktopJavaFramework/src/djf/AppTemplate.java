package djf;

import djf.ui.*;
import djf.components.*;
import javafx.application.Application;
import javafx.stage.Stage;
import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;
import properties_manager.PropertiesManager;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;
import properties_manager.InvalidXMLFileFormatException;

/**
 * This is the framework's JavaFX application. It provides the start method
 * that begins the program initialization, which delegates component
 * initialization to the application-specific child class' hook function.
 * 
 * @author Richard McKenna
 * @author Ralph Huang
 * @version 1.2
 */
public abstract class AppTemplate extends Application {

    //SHARED DEFAULT LANGUAGE
    protected String defaultLang = new String();
    // THIS IS THE APP'S FULL JavaFX GUI. NOTE THAT ALL APPS WOULD
    // SHARE A COMMON UI EXCEPT FOR THE CUSTOM WORKSPACE
    protected AppGUI gui;

    // THIS CLASS USES A COMPONENT ARCHITECTURE DESIGN PATTERN, MEANING IT
    // HAS OBJECTS THAT CAN BE SWAPPED OUT FOR OTHER COMPONENTS
    // THIS APP HAS 4 COMPONENTS
    
    // THE COMPONENT FOR MANAGING CUSTOM APP DATA
    protected AppDataComponent dataComponent;
    
    // THE COMPONENT FOR MANAGING CUSTOM FILE I/O
    protected AppFileComponent fileComponent;

    // THE COMPONENT FOR THE GUI WORKSPACE
    protected AppWorkspaceComponent workspaceComponent;
        
    // THIS METHOD MUST BE OVERRIDDEN WHERE THE CUSTOM BUILDER OBJECT
    // WILL PROVIDE THE CUSTOM APP COMPONENTS

    /**
     * This function must be overridden, it should initialize all
     * of the components used by the app in the proper order according
     * to the particular app's dependencies.
     */
    public abstract void buildAppComponentsHook();
    
    // COMPONENT ACCESSOR METHODS

    /**
     *  Accessor for the data component.
     */
    public AppDataComponent getDataComponent() { return dataComponent; }

    /**
     *  Accessor for the file component.
     */
    public AppFileComponent getFileComponent() { return fileComponent; }

    /**
     *  Accessor for the workspace component.
     */
    public AppWorkspaceComponent getWorkspaceComponent() { return workspaceComponent; }
    
    /**
     *  Accessor for the gui. Note that the GUI would contain the workspace.
     */
    public AppGUI getGUI() { return gui; }

    /**
     * Accessor for defaultLanguage
     */
    
    public String getDefaultLang()
    {
        /*Path file = Paths.get("C:\\Users\\ralph\\Documents\\NetBeansProjects\\hw2\\language.txt");
        try{
            BufferedReader reader = Files.newBufferedReader(file);
            defaultLang = reader.readLine();
            if(defaultLang == null)
                throw new Exception();
        }
        catch(Exception e)
        {
            System.out.println("No file found");
        }
        */
        return defaultLang;
    }
    
    /**
     * Mutator method for defaultLanguage
     */
    
    public void setDefaultLang(String value)
    {
        defaultLang = value;
    }
    /**
     * This is where our Application begins its initialization, it will load
     * the custom app properties, build the components, and fully initialize
     * everything to get the app rolling.
     *
     * @param primaryStage This application's window.
     */
    @Override
    public void start(Stage primaryStage) {
	// LET'S START BY INITIALIZING OUR DIALOGS
	AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
	messageDialog.init(primaryStage);
	AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
	yesNoDialog.init(primaryStage);
        LanguageChooserDialogSingleton languageDialog = LanguageChooserDialogSingleton.getSingleton();
        languageDialog.init(primaryStage);
        //Checks to see if there is a file for language
        Path file = Paths.get("C:\\Users\\ralph\\Documents\\NetBeansProjects\\hw2\\language.txt");
        try{
            BufferedReader reader = Files.newBufferedReader(file);
            defaultLang = reader.readLine();
            if(defaultLang == null)
                throw new Exception();
        }
        //Only prompt for language if there is no default language
        catch(Exception e)
        {
            try{
                BufferedWriter writer = Files.newBufferedWriter(file);
                languageDialog.show("Language", "Choose a language.");
                defaultLang = languageDialog.getSelection();
                if(defaultLang.equals("English") || defaultLang.equals("Japanese"))
                    {
                        //Saves language as a file
                        writer.write(defaultLang, 0, defaultLang.length());
                        writer.close();
                    }
            }
            catch(IOException ioexcep)
            {
                System.out.println("IOException");
            }
        }
        
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        
	try {
	    // LOAD APP PROPERTIES, BOTH THE BASIC UI STUFF FOR THE FRAMEWORK
	    // AND THE CUSTOM UI STUFF FOR THE WORKSPACE
            boolean success;
            //Verify default language before initializing                                  
            if (defaultLang.equals("English") )
            {
                success = loadProperties(APP_PROPERTIES_FILE_NAME_EN);
            }
            else if(defaultLang.equals("Japanese"))
            {
                success = loadProperties(APP_PROPERTIES_FILE_NAME_JA);
            }
	    else
                success = false;
                                
	    if (success) {
                // GET THE TITLE FROM THE XML FILE
                
		String appTitle = props.getProperty(APP_TITLE);
                
                // BUILD THE BASIC APP GUI OBJECT FIRST
		gui = new AppGUI(primaryStage, appTitle, this);

                // THIS BUILDS ALL OF THE COMPONENTS, NOTE THAT
                // IT WOULD BE DEFINED IN AN APPLICATION-SPECIFIC
                // CHILD CLASS
		buildAppComponentsHook();
                
                // NOW OPEN UP THE WINDOW
                primaryStage.show();
	    } 
            
	}catch (Exception e) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
	}
    }
    
    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     *
     * @param propertiesFileName The XML file containing properties to be
     * loaded in order to initialize the UI.
     * 
     * @return true if the properties file was loaded successfully, false
     * otherwise.
     */
    public boolean loadProperties(String propertiesFileName) {
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
	try {
	    // LOAD THE SETTINGS FOR STARTING THE APP
	    props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	    props.loadProperties(propertiesFileName, PROPERTIES_SCHEMA_FILE_NAME);
	    return true;
	} catch (InvalidXMLFileFormatException ixmlffe) {
	    // SOMETHING WENT WRONG INITIALIZING THE XML FILE
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
	    return false;
	}
    }
    }
