package gol.data;

/**
 * This enum has the various possible states of the logo maker app
 * during the editing process which helps us determine which controls
 * are usable or not and what specific user actions should affect.
 * 
 * @author Richard McKenna
 * @author Ralph Huang
 * @version 1.2
 */
public enum golState {
    SELECTING_SHAPE,
    DRAGGING_SHAPE,
    STARTING_RECTANGLE,
    STARTING_ELLIPSE,
    SIZING_SHAPE,
    DRAGGING_NOTHING,
    SIZING_NOTHING,
    DRAGGING_TEXT,
    STARTING_TEXT,
    STARTING_IMAGE,
}
