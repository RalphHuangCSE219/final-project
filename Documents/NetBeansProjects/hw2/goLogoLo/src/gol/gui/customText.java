/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

/**
 *
 * @author ralph
 */
class customText {
    String text;
    String fontType;
    String fontSize;
    boolean italic;
    boolean bold;
    
    public String getText(){
        return text;
    }
    
    public String getFontType(){
        return fontType;
    }
    
    public String getFontSize(){
        return fontSize;
    }
    
    public boolean isItalic(){
        return italic;
    }
    
    public boolean isBold(){
        return bold;
    }
    
    public void setText(String newText){
        text = newText;
    }
    
    public void setFontType(String newFontType){
        fontType = newFontType;
    }
    
    public void setFontSize(String newFontSize){
        fontSize = newFontSize;
    }
    
    public void setItalic(boolean tf){
        italic = tf;
    }
    
    public void setBold(boolean tf){
        bold = tf;
    }
}
