package gol.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import static gol.golLanguageProperty.TEXT_ICON;
import static gol.golLanguageProperty.TEXT_TOOLTIP;
import static gol.golLanguageProperty.IMAGE_ICON;
import static gol.golLanguageProperty.IMAGE_TOOLTIP;
import static gol.golLanguageProperty.ELLIPSE_ICON;
import static gol.golLanguageProperty.ELLIPSE_TOOLTIP;
import static gol.golLanguageProperty.MOVE_TO_BACK_ICON;
import static gol.golLanguageProperty.MOVE_TO_BACK_TOOLTIP;
import static gol.golLanguageProperty.MOVE_TO_FRONT_ICON;
import static gol.golLanguageProperty.MOVE_TO_FRONT_TOOLTIP;
import static gol.golLanguageProperty.RECTANGLE_ICON;
import static gol.golLanguageProperty.RECTANGLE_TOOLTIP;
import static gol.golLanguageProperty.REMOVE_ICON;
import static gol.golLanguageProperty.REMOVE_ELEMENT_TOOLTIP;
import static gol.golLanguageProperty.SELECTION_TOOL_ICON;
import static gol.golLanguageProperty.SELECTION_TOOL_TOOLTIP;
import static gol.golLanguageProperty.SNAPSHOT_ICON;
import static gol.golLanguageProperty.SNAPSHOT_TOOLTIP;
import gol.data.golData;
import static gol.data.golData.BLACK_HEX;
import static gol.data.golData.WHITE_HEX;
import gol.data.golState;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME_EN;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME_JA;
import djf.ui.LanguageChooserDialogSingleton;
import static gol.css.golStyle.*;
import static djf.settings.AppPropertyType.*;
import static gol.golLanguageProperty.BACKGROUND_TOOLTIP;
import static gol.golLanguageProperty.FILL_TOOLTIP;
import static gol.golLanguageProperty.OUTLINE_TOOLTIP;
import javafx.scene.control.Tooltip;
import properties_manager.PropertiesManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author Ralph Huang
 * @version 1.2
 */
public class golWorkspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // HAS ALL THE CONTROLS FOR EDITING
    VBox editToolbar;

    // FIRST ROW
    HBox row1Box;
    Button selectionToolButton;
    Button removeButton;
    Button rectButton;
    Button ellipseButton;

    HBox rowITBox;
    Button addImageButton;
    Button addTextButton;

    // SECOND ROW
    HBox row2Box;
    Button moveToBackButton;
    Button moveToFrontButton;

    // THIRD ROW
    VBox row3Box;
    Label backgroundColorLabel;
    ColorPicker backgroundColorPicker;

    // FORTH ROW
    VBox row4Box;
    Label fillColorLabel;
    ColorPicker fillColorPicker;

    // FIFTH ROW
    VBox row5Box;
    Label outlineColorLabel;
    ColorPicker outlineColorPicker;

    // SIXTH ROW
    VBox row6Box;
    Label outlineThicknessLabel;
    Slider outlineThicknessSlider;

    // SEVENTH ROW
    HBox row7Box;
    Button snapshotButton;

    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;

    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    LogoEditController logoEditController;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    LanguageChooserDialogSingleton languageDialog;

    // FOR DISPLAYING DEBUG STUFF
    Text debugText;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public golWorkspace(AppTemplate initApp) {
        // KEEP THIS FOR LATER

        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }

    /**
     * Note that this is for displaying text during development.
     * @param text
     */
    public void setDebugText(String text) {
        debugText.setText(text);
    }

    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    public ColorPicker getFillColorPicker() {
        return fillColorPicker;
    }

    public ColorPicker getOutlineColorPicker() {
        return outlineColorPicker;
    }

    public ColorPicker getBackgroundColorPicker() {
        return backgroundColorPicker;
    }

    public Slider getOutlineThicknessSlider() {
        return outlineThicknessSlider;
    }

    public Pane getCanvas() {
        return canvas;
    }

    // HELPER SETUP METHOD
    private void initLayout() {
        // THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
        editToolbar = new VBox();

        // ROW 1
        row1Box = new HBox();
        selectionToolButton = gui.initChildButton(row1Box, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), true);
        removeButton = gui.initChildButton(row1Box, REMOVE_ICON.toString(), REMOVE_ELEMENT_TOOLTIP.toString(), true);
        rectButton = gui.initChildButton(row1Box, RECTANGLE_ICON.toString(), RECTANGLE_TOOLTIP.toString(), false);
        ellipseButton = gui.initChildButton(row1Box, ELLIPSE_ICON.toString(), ELLIPSE_TOOLTIP.toString(), false);

        //ROW 2.5
        rowITBox = new HBox();
        addTextButton = gui.initChildButton(rowITBox, TEXT_ICON.toString(), TEXT_TOOLTIP.toString(), false);
        addImageButton = gui.initChildButton(rowITBox, IMAGE_ICON.toString(), IMAGE_TOOLTIP.toString(), false);

        // ROW 2
        row2Box = new HBox();
        moveToBackButton = gui.initChildButton(row2Box, MOVE_TO_BACK_ICON.toString(), MOVE_TO_BACK_TOOLTIP.toString(), true);
        moveToFrontButton = gui.initChildButton(row2Box, MOVE_TO_FRONT_ICON.toString(), MOVE_TO_FRONT_TOOLTIP.toString(), true);

        // ROW 3
        row3Box = new VBox();
        if (app.getDefaultLang().equals("English")) {
            backgroundColorLabel = new Label("Background Color");
        } else if (app.getDefaultLang().equals("Japanese")) {
            backgroundColorLabel = new Label("背景色");
        }
        backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
        row3Box.getChildren().add(backgroundColorLabel);
        row3Box.getChildren().add(backgroundColorPicker);

        // ROW 4
        row4Box = new VBox();
        if (app.getDefaultLang().equals("English")) {
            fillColorLabel = new Label("Fill Color");
        } else if (app.getDefaultLang().equals("Japanese")) {
            fillColorLabel = new Label("塗りつぶし色");
        }
        fillColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
        row4Box.getChildren().add(fillColorLabel);
        row4Box.getChildren().add(fillColorPicker);

        // ROW 5
        row5Box = new VBox();
        if (app.getDefaultLang().equals("English")) {
            outlineColorLabel = new Label("Outline Color");
        } else if (app.getDefaultLang().equals("Japanese")) {
            outlineColorLabel = new Label("アウトラインの色");
        }
        outlineColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
        row5Box.getChildren().add(outlineColorLabel);
        row5Box.getChildren().add(outlineColorPicker);

        // ROW 6
        row6Box = new VBox();
        if (app.getDefaultLang().equals("English")) {
            outlineThicknessLabel = new Label("Outline Thickness");
        } else if (app.getDefaultLang().equals("Japanese")) {
            outlineThicknessLabel = new Label("アウトラインの太さ");
        }
        outlineThicknessSlider = new Slider(0, 10, 1);
        row6Box.getChildren().add(outlineThicknessLabel);
        row6Box.getChildren().add(outlineThicknessSlider);

        // ROW 7
        row7Box = new HBox();
        snapshotButton = gui.initChildButton(row7Box, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);

        // NOW ORGANIZE THE EDIT TOOLBAR
        editToolbar.getChildren().add(row1Box);
        editToolbar.getChildren().add(rowITBox);
        editToolbar.getChildren().add(row2Box);
        editToolbar.getChildren().add(row3Box);
        editToolbar.getChildren().add(row4Box);
        editToolbar.getChildren().add(row5Box);
        editToolbar.getChildren().add(row6Box);
        editToolbar.getChildren().add(row7Box);

        // WE'LL RENDER OUR STUFF HERE IN THE CANVAS
        canvas = new Pane();
        debugText = new Text();
        canvas.getChildren().add(debugText);
        debugText.setX(100);
        debugText.setY(100);

        // AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
        golData data = (golData) app.getDataComponent();
        data.setShapes(canvas.getChildren());

        // AND NOW SETUP THE WORKSPACE
        workspace = new BorderPane();
        ((BorderPane) workspace).setLeft(editToolbar);
        ((BorderPane) workspace).setCenter(canvas);
    }

    // HELPER SETUP METHOD
    private void initControllers() {
        // MAKE THE EDIT CONTROLLER
        golData data = (golData) app.getDataComponent();
        logoEditController = new LogoEditController(app);

        // NOW CONNECT THE BUTTONS TO THEIR HANDLERS
        selectionToolButton.setOnAction(e -> {
            logoEditController.processSelectSelectionTool();
        });
        removeButton.setOnAction(e -> {
            logoEditController.processRemoveSelectedShape();
        });
        rectButton.setOnAction(e -> {
            logoEditController.processSelectRectangleToDraw();
        });
        ellipseButton.setOnAction(e -> {
            logoEditController.processSelectEllipseToDraw();
        });

        addTextButton.setOnAction(e -> {
            logoEditController.processSelectTextToCreate();
            canvasController.processAddText();
        });
        addImageButton.setOnAction(e -> {
            canvasController.processAddImage();
            logoEditController.processSelectImageToCreate();
        });

        moveToBackButton.setOnAction(e -> {
            logoEditController.processMoveSelectedShapeToBack();
        });
        moveToFrontButton.setOnAction(e -> {
            logoEditController.processMoveSelectedShapeToFront();
        });

        backgroundColorPicker.setOnAction(e -> {
            logoEditController.processSelectBackgroundColor();
        });
        fillColorPicker.setOnAction(e -> {
            logoEditController.processSelectFillColor();
        });
        outlineColorPicker.setOnAction(e -> {
            logoEditController.processSelectOutlineColor();
        });
        outlineThicknessSlider.valueProperty().addListener(e -> {
            logoEditController.processSelectOutlineThickness();
        });
        snapshotButton.setOnAction(e -> {
            logoEditController.processSnapshot();
        });

        gui.getLangButton().setOnAction(e -> {
            gui.getFileController().handleLangRequest();
            changeLang();
        });
        
        gui.getCutButton().setOnAction(e -> {
           canvasController.handleCutRequest(); 
        });
        
        gui.getCopyButton().setOnAction(e -> {
            canvasController.handleCopyRequest();
        });
        
        gui.getPasteButton().setOnAction(e -> {
           canvasController.handlePasteRequest(); 
        });
        
        gui.getUndoButton().setOnAction(e -> {
            data.getJTPS().undoTransaction();
        });
        
        gui.getRedoButton().setOnAction(e -> {
            data.getJTPS().doTransaction();
        });

        // MAKE THE CANVAS CONTROLLER	
        canvasController = new CanvasController(app);
        
        canvas.setOnMousePressed(e -> {
            int x = e.getClickCount();
            canvasController.processCanvasMousePress((int) e.getX(), (int) e.getY(), x);
        });
        canvas.setOnMouseReleased(e -> {
            canvasController.processCanvasMouseRelease((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseDragged(e -> {
            canvasController.processCanvasMouseDragged((int) e.getX(), (int) e.getY());
        });
    }

    // HELPER METHOD
    public void loadSelectedShapeSettings(Shape shape) {
        if (shape != null) {
            try {
                Color fillColor = (Color) shape.getFill();
                Color strokeColor = (Color) shape.getStroke();
                double lineThickness = shape.getStrokeWidth();
                fillColorPicker.setValue(fillColor);
                outlineColorPicker.setValue(strokeColor);
                outlineThicknessSlider.setValue(lineThickness);
                fillColorPicker.setDisable(false);
                app.getGUI().getCutButton().setDisable(false);
                app.getGUI().getCopyButton().setDisable(false);
                app.getGUI().getPasteButton().setDisable(canvasController.tempShape == null);
            } catch (Exception e) {
               // fillColorPicker.setValue(null);
                fillColorPicker.setDisable(true);
            }
        }

    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE
        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);

        // COLOR PICKER STYLE
        fillColorPicker.getStyleClass().add(CLASS_BUTTON);
        outlineColorPicker.getStyleClass().add(CLASS_BUTTON);
        backgroundColorPicker.getStyleClass().add(CLASS_BUTTON);

        editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        row1Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        rowITBox.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row2Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row3Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        backgroundColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);

        row4Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        fillColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        row5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        outlineColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        row6Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        outlineThicknessLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        row7Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
    }

    /**
     * This function reloads all the controls for editing logos the workspace.
     * @param data
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
        golData dataManager = (golData) data;
        if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
            selectionToolButton.setDisable(false);
            removeButton.setDisable(true);
            rectButton.setDisable(true);
            ellipseButton.setDisable(false);
            fillColorPicker.setValue(Color.WHITE);
            fillColorPicker.setDisable(false);
        } else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
            selectionToolButton.setDisable(false);
            removeButton.setDisable(true);
            rectButton.setDisable(false);
            ellipseButton.setDisable(true);
            fillColorPicker.setDisable(false);
        } else if (dataManager.isInState(golState.SELECTING_SHAPE)
                || dataManager.isInState(golState.DRAGGING_SHAPE)
                || dataManager.isInState(golState.DRAGGING_NOTHING)) {
            boolean shapeIsNotSelected = dataManager.getSelectedShape() == null;
            selectionToolButton.setDisable(true);
            removeButton.setDisable(shapeIsNotSelected);
            rectButton.setDisable(false);
            ellipseButton.setDisable(false);
            moveToFrontButton.setDisable(shapeIsNotSelected);
            moveToBackButton.setDisable(shapeIsNotSelected);
        }
        app.getGUI().getCutButton().setDisable(dataManager.getSelectedShape() == null);
        app.getGUI().getCopyButton().setDisable(dataManager.getSelectedShape() == null);
        app.getGUI().getPasteButton().setDisable(canvasController.tempShape == null);
        removeButton.setDisable(dataManager.getSelectedShape() == null);
        backgroundColorPicker.setValue(dataManager.getBackgroundColor());
    }

    @Override
    public void resetWorkspace() {
        // WE ARE NOT USING THIS, THOUGH YOU MAY IF YOU LIKE
    }

    public void changeLang() {
        if (app.getDefaultLang().equals("English")) {
            backgroundColorLabel.setText("Background Color");
            outlineColorLabel.setText("Outline Color");
            fillColorLabel.setText("Fill Color");
            outlineThicknessLabel.setText("Outline Thickness");
            app.loadProperties(APP_PROPERTIES_FILE_NAME_EN);

        } else if (app.getDefaultLang().equals("Japanese")) {
            backgroundColorLabel.setText("背景色");
            outlineColorLabel.setText("塗りつぶし色");
            fillColorLabel.setText("アウトラインの色");
            outlineThicknessLabel.setText("アウトラインの太さ");
            app.loadProperties(APP_PROPERTIES_FILE_NAME_JA);
        }

        changeToolTipLang();

    }

    /**
     * Helper method to change the tooltip values
     */
    public void changeToolTipLang() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Tooltip buttonTooltip = new Tooltip(props.getProperty(LANG_TOOLTIP));
        app.getGUI().getLangButton().setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(NEW_TOOLTIP));
        app.getGUI().getNewButton().setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(SAVE_TOOLTIP));
        app.getGUI().getSaveButton().setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(LOAD_TOOLTIP));
        app.getGUI().getLoadButton().setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(EXIT_TOOLTIP));
        app.getGUI().getExitButton().setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(ABOUT_TOOLTIP));
        app.getGUI().getAboutButton().setTooltip(buttonTooltip);
        
        buttonTooltip = new Tooltip(props.getProperty(CUT_TOOLTIP));
        app.getGUI().getCutButton().setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(COPY_TOOLTIP));
        app.getGUI().getCopyButton().setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(PASTE_TOOLTIP));
        app.getGUI().getPasteButton().setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(SNAPSHOT_TOOLTIP));
        snapshotButton.setTooltip(buttonTooltip);
        
        buttonTooltip = new Tooltip(props.getProperty(SELECTION_TOOL_TOOLTIP));
        selectionToolButton.setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(REMOVE_ELEMENT_TOOLTIP));
        removeButton.setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(RECTANGLE_TOOLTIP));
        rectButton.setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(ELLIPSE_TOOLTIP));
        ellipseButton.setTooltip(buttonTooltip);
        

        buttonTooltip = new Tooltip(props.getProperty(IMAGE_TOOLTIP));
        addImageButton.setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(TEXT_TOOLTIP));
        addTextButton.setTooltip(buttonTooltip);

        buttonTooltip = new Tooltip(props.getProperty(MOVE_TO_BACK_TOOLTIP));
        moveToBackButton.setTooltip(buttonTooltip);
        buttonTooltip = new Tooltip(props.getProperty(MOVE_TO_FRONT_TOOLTIP));
        moveToFrontButton.setTooltip(buttonTooltip);
    }
}
