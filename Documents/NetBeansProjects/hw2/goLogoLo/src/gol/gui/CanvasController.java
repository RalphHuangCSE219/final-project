package gol.gui;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;
import gol.data.golData;
import gol.data.Draggable;
import gol.data.golState;
import static gol.data.golState.DRAGGING_NOTHING;
import static gol.data.golState.DRAGGING_SHAPE;
import static gol.data.golState.SELECTING_SHAPE;
import static gol.data.golState.SIZING_SHAPE;
import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableImage;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import java.io.File;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author Richard McKenna
 * @author Ralph Huang
 * @version 1.2
 */
public class CanvasController {

    AppTemplate app;
    DraggableText testText = new DraggableText();
    DraggableImage testImage = new DraggableImage();
    Shape tempShape;
    DraggableImage tempDraggableImage;
    DraggableText editText = new DraggableText();

    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }

    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMousePress(int x, int y, int mouseClicks) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();

            Draggable testShape1 = (Draggable) shape;
            try {
                if (testShape1.getShapeType().equals("TEXT") && mouseClicks == 2) {
                    DraggableText testShape2 = (DraggableText) shape;
                    Dialog<customText> textOptions = new Dialog<>();
                    textOptions.setTitle("Edit Text");
                    textOptions.setHeaderText(null);
                    textOptions.setResizable(false);

                    Label ftextLabel = new Label("Edit your text");
                    TextField textField = new TextField();
                    textField.setText(testShape2.getText());
                    //ComboBoxes
                    Label fsizeLabel = new Label("Font Size");
                    ComboBox<Integer> fontSizeBox = new ComboBox<>();
                    fontSizeBox.getItems().addAll(
                            9,
                            10,
                            11,
                            12,
                            14,
                            16,
                            18,
                            20,
                            22,
                            25,
                            28,
                            36,
                            42,
                            48,
                            54,
                            60,
                            72
                    );
                    fontSizeBox.getSelectionModel().select((int) testShape2.getFont().getSize());

                    Label ftypeLabel = new Label("Font Type");
                    ComboBox<String> fontTypeBox = new ComboBox<>();
                    fontTypeBox.getItems().addAll(
                            "Arial",
                            "Calibri",
                            "Cambria",
                            "Comic Sans",
                            "Helvetica",
                            "Times New Roman"
                    );
                    fontTypeBox.getSelectionModel().select(testShape2.getFont().getFamily());

                    CheckBox boldCheckBox = new CheckBox("Bold");
                    CheckBox italicCheckBox = new CheckBox("Italic");
                    boldCheckBox.setSelected(testShape2.getFont().getName().contains("Bold"));
                    italicCheckBox.setSelected(testShape2.getFont().getName().contains("Italic"));

                    //ORGANIZE COMPONENETS IN A GRID
                    GridPane grid = new GridPane();
                    grid.setVgap(4);
                    grid.setHgap(8);
                    grid.add(ftextLabel, 1, 1);
                    grid.add(textField, 2, 1);
                    grid.add(ftypeLabel, 1, 2);
                    grid.add(fontTypeBox, 2, 2);
                    grid.add(fsizeLabel, 1, 3);
                    grid.add(fontSizeBox, 2, 3);
                    grid.add(boldCheckBox, 3, 1);
                    grid.add(italicCheckBox, 3, 2);
                    grid.setPadding(new Insets(20, 50, 10, 10));

                    textOptions.getDialogPane().setContent(grid);
                    textOptions.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

                    textOptions.showAndWait();
                    Alert noText = new Alert(Alert.AlertType.WARNING);
                    noText.setTitle("No Text Warning");
                    noText.setContentText("You did not enter anything in the textBox");
                    DraggableText newText = new DraggableText();
                    scene = app.getGUI().getPrimaryScene();
                    if (textField.getText().equals("") || textField.getText() == null) {

                        scene.setCursor(Cursor.DEFAULT);
                        noText.showAndWait();
                    } else {
                        if (fontTypeBox.getSelectionModel().getSelectedItem() == null) {
                            Alert noFont = new Alert(Alert.AlertType.WARNING);
                            noFont.setTitle("No Text Warning");
                            noFont.setContentText("You did not enter anything in the fontBox");
                            scene.setCursor(Cursor.DEFAULT);
                            noFont.showAndWait();
                            return;
                        }

                        if (fontSizeBox.getSelectionModel().getSelectedItem() == null) {
                            Alert noSize = new Alert(Alert.AlertType.WARNING);
                            noSize.setTitle("No Text Warning");
                            noSize.setContentText("You did not enter anything in the sizeBox");
                            scene.setCursor(Cursor.DEFAULT);
                            noSize.showAndWait();
                            return;
                        }

                        newText.setText(textField.getText());

                        if (boldCheckBox.isSelected() && italicCheckBox.isSelected()) {
                            newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.BOLD, FontPosture.ITALIC, fontSizeBox.getSelectionModel().getSelectedItem()));
                        } else if (boldCheckBox.isSelected()) {
                            newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.BOLD, FontPosture.REGULAR, fontSizeBox.getSelectionModel().getSelectedItem()));
                        } else if (italicCheckBox.isSelected()) {
                            newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.NORMAL, FontPosture.ITALIC, fontSizeBox.getSelectionModel().getSelectedItem()));
                        } else {
                            newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.NORMAL, FontPosture.REGULAR, fontSizeBox.getSelectionModel().getSelectedItem()));
                        }
                        editText = newText;
                        editText.setX(testShape1.getX());
                        editText.setY(testShape1.getY());
                        dataManager.removeShape(shape);
                        dataManager.startNewText((int) editText.getX(), (int) editText.getY(), editText);
                    }
                }
            } catch (NullPointerException e) {

            }

            // AND START DRAGGING IT
            if (shape != null) {
                scene.setCursor(Cursor.MOVE);
                dataManager.setState(golState.DRAGGING_SHAPE);
                app.getGUI().updateToolbarControls(false);
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        } else if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
            dataManager.startNewRectangle(x, y);
        } else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
            dataManager.startNewEllipse(x, y);
        } else if (dataManager.isInState(golState.STARTING_TEXT)) {
            dataManager.startNewText(x, y, testText);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
        } else if (dataManager.isInState(golState.STARTING_IMAGE)) {
            dataManager.startNewImage(x, y, testImage);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
        }
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseDragged(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            Draggable newDraggableShape = (Draggable) dataManager.getNewShape();
            newDraggableShape.size(x, y);
        } else if (dataManager.isInState(DRAGGING_SHAPE)) {
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            try {
                selectedDraggableShape.drag(x, y);
            } catch (NullPointerException e) {

            }
            app.getGUI().updateToolbarControls(false);

        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call
     * canvas, but is actually a Pane.
     */
    public void processCanvasMouseRelease(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            dataManager.selectSizedShape();
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(golState.DRAGGING_SHAPE)) {
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(golState.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_SHAPE);
        }
    }

    //Prompts a dialog to create text
    public void processAddText() {

        Dialog<customText> textOptions = new Dialog<>();
        textOptions.setTitle("Create Text");
        textOptions.setHeaderText(null);
        textOptions.setResizable(false);

        Label ftextLabel = new Label("Enter your text");
        TextField textField = new TextField();

        //ComboBoxes
        Label fsizeLabel = new Label("Font Size");
        ComboBox<Integer> fontSizeBox = new ComboBox<>();
        fontSizeBox.getItems().addAll(
                9,
                10,
                11,
                12,
                14,
                16,
                18,
                20,
                22,
                25,
                28,
                36,
                42,
                48,
                54,
                60,
                72
        );
        fontSizeBox.setPromptText("Choose a font size");

        Label ftypeLabel = new Label("Font Type");
        ComboBox<String> fontTypeBox = new ComboBox<>();
        fontTypeBox.getItems().addAll(
                "Arial",
                "Calibri",
                "Cambria",
                "Helvetica",
                "Times New Roman"
        );
        fontTypeBox.setPromptText("Choose a font type");

        CheckBox boldCheckBox = new CheckBox("Bold");
        CheckBox italicCheckBox = new CheckBox("Italic");

        //ORGANIZE COMPONENETS IN A GRID
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(8);
        grid.add(ftextLabel, 1, 1);
        grid.add(textField, 2, 1);
        grid.add(ftypeLabel, 1, 2);
        grid.add(fontTypeBox, 2, 2);
        grid.add(fsizeLabel, 1, 3);
        grid.add(fontSizeBox, 2, 3);
        grid.add(boldCheckBox, 3, 1);
        grid.add(italicCheckBox, 3, 2);
        grid.setPadding(new Insets(20, 50, 10, 10));

        textOptions.getDialogPane().setContent(grid);
        textOptions.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        (ButtonType.OK).getButtonData().isDefaultButton();
        textOptions.showAndWait();
        Alert noText = new Alert(Alert.AlertType.WARNING);
        noText.setTitle("No Text Warning");
        noText.setContentText("You did not enter anything in the textBox");
        DraggableText newText = new DraggableText();
        Scene scene = app.getGUI().getPrimaryScene();
        if (textField.getText().equals("") || textField.getText() == null) {

            scene.setCursor(Cursor.DEFAULT);
            noText.showAndWait();
        } else {
            if (fontTypeBox.getSelectionModel().getSelectedItem() == null) {
                Alert noFont = new Alert(Alert.AlertType.WARNING);
                noFont.setTitle("No Text Warning");
                noFont.setContentText("You did not enter anything in the fontBox");
                scene.setCursor(Cursor.DEFAULT);
                noFont.showAndWait();
                return;
            }

            if (fontSizeBox.getSelectionModel().getSelectedItem() == null) {
                Alert noSize = new Alert(Alert.AlertType.WARNING);
                noSize.setTitle("No Text Warning");
                noSize.setContentText("You did not enter anything in the sizeBox");
                scene.setCursor(Cursor.DEFAULT);
                noSize.showAndWait();
                return;
            }

            newText.setText(textField.getText());

            if (boldCheckBox.isSelected() && italicCheckBox.isSelected()) {
                newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.BOLD, FontPosture.ITALIC, fontSizeBox.getSelectionModel().getSelectedItem()));
            } else if (boldCheckBox.isSelected()) {
                newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.BOLD, FontPosture.REGULAR, fontSizeBox.getSelectionModel().getSelectedItem()));
            } else if (italicCheckBox.isSelected()) {
                newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.NORMAL, FontPosture.ITALIC, fontSizeBox.getSelectionModel().getSelectedItem()));
            } else {
                newText.setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.NORMAL, FontPosture.REGULAR, fontSizeBox.getSelectionModel().getSelectedItem()));
            }

            testText = newText;
        }
    }

    void processAddImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose Image File");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                new ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(app.getGUI().getWindow());
        if (selectedFile != null) {
            Image selectedImage = new Image("file:///" + selectedFile.getAbsoluteFile());
            DraggableImage image = new DraggableImage();
            image.setFilePath("file:///" + selectedFile.getAbsoluteFile());
            image.setWidth(selectedImage.getWidth());
            image.setHeight(selectedImage.getHeight());
            image.setFill(new ImagePattern(selectedImage));
            testImage = image;
            testImage.setFilePath(image.getFilePath());
        }
    }

    void handleCutRequest() {
        golData dataManager = (golData) app.getDataComponent();
        tempShape = dataManager.getSelectedShape();
        try {
            DraggableImage test = (DraggableImage) dataManager.getSelectedShape();
            ((DraggableImage) tempShape).setFilePath(test.getFilePath());
        } catch (Exception e) {

        }
        dataManager.removeSelectedShape();

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void handleCopyRequest() {
        golData dataManager = (golData) app.getDataComponent();
        tempShape = dataManager.getSelectedShape();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void handlePasteRequest() {
        Draggable storedShape = (Draggable) tempShape;
        golData dataManager = (golData) app.getDataComponent();
        if (storedShape.getShapeType().equals("RECTANGLE")) {
            DraggableRectangle pastedShape = new DraggableRectangle();
            storedShape = (DraggableRectangle) tempShape;
            pastedShape.setFill(tempShape.getFill());
            pastedShape.setStroke(tempShape.getStroke());
            pastedShape.setStrokeWidth(tempShape.getStrokeWidth());
            pastedShape.setLocationAndSize(storedShape.getX() + 10, storedShape.getY() + 10, storedShape.getWidth(), storedShape.getHeight());
            dataManager.getShapes().add(pastedShape);
            tempShape = pastedShape;
        } else if (storedShape.getShapeType().equals("ELLIPSE")) {
            DraggableEllipse pastedShape = new DraggableEllipse();
            storedShape = (DraggableEllipse) tempShape;
            pastedShape.setFill(tempShape.getFill());
            pastedShape.setStroke(tempShape.getStroke());
            pastedShape.setStrokeWidth(tempShape.getStrokeWidth());
            pastedShape.setLocationAndSize(storedShape.getX() + 10, storedShape.getY() + 10, storedShape.getWidth(), storedShape.getHeight());
            dataManager.getShapes().add(pastedShape);
            tempShape = pastedShape;
        } else if (storedShape.getShapeType().equals("TEXT")) {
            DraggableText pastedShape = new DraggableText();
            DraggableText copiedShape = (DraggableText) tempShape;
            pastedShape.setFill(tempShape.getFill());
            pastedShape.setStroke(tempShape.getStroke());
            pastedShape.setStrokeWidth(tempShape.getStrokeWidth());
            pastedShape.setText(copiedShape.getText());
            pastedShape.setFont(copiedShape.getFont());
            pastedShape.setLocationAndSize(copiedShape.getX() + 10, copiedShape.getY() + 10, copiedShape.getWidth(), copiedShape.getHeight());
            dataManager.getShapes().add(pastedShape);
            tempShape = pastedShape;
        } else if (storedShape.getShapeType().equals("IMAGE")) {
            DraggableImage pastedShape = new DraggableImage();
            storedShape = (DraggableImage) tempShape;
            pastedShape.setFill(tempShape.getFill());
            pastedShape.setStroke(tempShape.getStroke());
            pastedShape.setStrokeWidth(tempShape.getStrokeWidth());
            pastedShape.setLocationAndSize(storedShape.getX() + 10, storedShape.getY() + 10, storedShape.getWidth(), storedShape.getHeight());
            pastedShape.setFilePath(((DraggableImage) storedShape).getFilePath());
            dataManager.getShapes().add(pastedShape);
            tempShape = pastedShape;
        } else {
            System.out.println("Error in shape type");
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Shape getTempShape() {
        return tempShape;
    }
}
