/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm;

/**
 *
 * @author ralph
 */
public enum mmmLanguageProperty {
    ADDLINE_ICON,
    ADDLINE_TOOLTIP,
    
    REMOVELINE_ICON,
    REMOVELINE_TOOLTIP,
    
    ADDSTATION_TOLINE_ICON,
    ADDSTATION_TOLINE_TOOLTIP,
    
    REMOVESTATION_FROMLINE_ICON,
    REMOVESTATION_FROMLINE_TOOLTIP,
    
    ADDSTATION_ICON,
    ADDSTATION_TOOLTIP,
    
    REMOVESTATION_ICON,
    REMOVESTATION_TOOLTIP,
    
    EDIT_LINE_ICON,
    EDIT_LINE_TOOLTIP,
    
    LIST_ICON,
    LIST_TOOLTIP,
    
    BOLD_ICON,
    BOLD_TOOLTIP,
    
    ITALICIZE_ICON,
    ITALICIZE_TOOLTIP,
    
    SNAPGRID_ICON,
    SNAPGRID_TOOLTIP,
    
    MOVELABEL_ICON,
    MOVELABEL_TOOLTIP,
    
    ROUTE_ICON,
    ROUTE_TOOLTIP,
    
    ROTATE_ICON,
    ROTATE_TOOLTIP,
    
    ZOOM_IN_ICON,
    ZOOM_IN_TOOLTIP,
    
    ZOOM_OUT_ICON,
    ZOOM_OUT_TOOLTIP,
    
    MAP_BIG_ICON,
    MAP_BIG_TOOLTIP,
    
    MAP_SMALL_ICON,
    MAP_SMALL_TOOLTIP,
}
