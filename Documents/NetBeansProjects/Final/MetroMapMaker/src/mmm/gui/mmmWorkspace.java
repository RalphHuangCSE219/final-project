/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import static mmm.css.mmmStyle.CLASS_BUTTON;
import static mmm.css.mmmStyle.CLASS_COLOR_CHOOSER_CONTROL;
import static mmm.css.mmmStyle.CLASS_EDIT_TOOLBAR;
import static mmm.css.mmmStyle.CLASS_EDIT_TOOLBAR_ROW;
import static mmm.css.mmmStyle.CLASS_RENDER_CANVAS;
import mmm.data.DraggableCircle;
import mmm.data.DraggableImage;
import mmm.data.DraggableLine;
import mmm.data.DraggableLineWrapper;
import mmm.data.DraggableText;
import mmm.data.mmmData;
import static mmm.data.mmmData.WHITE_HEX;
import static mmm.mmmLanguageProperty.ADDLINE_ICON;
import static mmm.mmmLanguageProperty.ADDLINE_TOOLTIP;
import static mmm.mmmLanguageProperty.ADDSTATION_ICON;
import static mmm.mmmLanguageProperty.ADDSTATION_TOLINE_ICON;
import static mmm.mmmLanguageProperty.ADDSTATION_TOLINE_TOOLTIP;
import static mmm.mmmLanguageProperty.ADDSTATION_TOOLTIP;
import static mmm.mmmLanguageProperty.BOLD_ICON;
import static mmm.mmmLanguageProperty.BOLD_TOOLTIP;
import static mmm.mmmLanguageProperty.EDIT_LINE_ICON;
import static mmm.mmmLanguageProperty.EDIT_LINE_TOOLTIP;
import static mmm.mmmLanguageProperty.ITALICIZE_ICON;
import static mmm.mmmLanguageProperty.ITALICIZE_TOOLTIP;
import static mmm.mmmLanguageProperty.LIST_ICON;
import static mmm.mmmLanguageProperty.LIST_TOOLTIP;
import static mmm.mmmLanguageProperty.MAP_BIG_ICON;
import static mmm.mmmLanguageProperty.MAP_BIG_TOOLTIP;
import static mmm.mmmLanguageProperty.MAP_SMALL_ICON;
import static mmm.mmmLanguageProperty.MAP_SMALL_TOOLTIP;
import static mmm.mmmLanguageProperty.MOVELABEL_ICON;
import static mmm.mmmLanguageProperty.MOVELABEL_TOOLTIP;
import static mmm.mmmLanguageProperty.REMOVELINE_ICON;
import static mmm.mmmLanguageProperty.REMOVELINE_TOOLTIP;
import static mmm.mmmLanguageProperty.REMOVESTATION_ICON;
import static mmm.mmmLanguageProperty.REMOVESTATION_TOOLTIP;
import static mmm.mmmLanguageProperty.ROTATE_ICON;
import static mmm.mmmLanguageProperty.ROTATE_TOOLTIP;
import static mmm.mmmLanguageProperty.ROUTE_ICON;
import static mmm.mmmLanguageProperty.ROUTE_TOOLTIP;
import static mmm.mmmLanguageProperty.SNAPGRID_ICON;
import static mmm.mmmLanguageProperty.SNAPGRID_TOOLTIP;
import static mmm.mmmLanguageProperty.ZOOM_IN_ICON;
import static mmm.mmmLanguageProperty.ZOOM_IN_TOOLTIP;
import static mmm.mmmLanguageProperty.ZOOM_OUT_ICON;
import static mmm.mmmLanguageProperty.ZOOM_OUT_TOOLTIP;

import properties_manager.PropertiesManager;

/**
 *
 * @author ralph
 */
public class mmmWorkspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    //Editing toolbar
    VBox editToolbar;

    VBox row1Box;
    //FIRST ROW
    HBox lineBox;
    Label metroLineLabel;
    ComboBox lines;
    ColorPicker lineColorPicker;

    HBox row1ButtonBox;
    Button addLineButton;
    Button removeLineButton;
    Button editLineButton;
    Button addStationToLineButton;
    Button removeStationFromLineButton;
    Button listStationsButton;

    HBox row1SliderBox;
    Slider outlineThicknessSlider;

    VBox row2Box;
    //SECOND ROW
    HBox stationBox;
    Label metroStationLabel;
    ComboBox stations;
    ColorPicker stationColorPicker;

    HBox row2ButtonBox;
    Button newStationButton;
    Button removeStationButton;
    Button snapButton;
    Button moveLabelButton;
    Button rotateLabelButton;

    HBox row2SliderBox;
    Slider radiusSlider;

    HBox row3Box;
    //THIRD ROW
    VBox routeBox;
    ComboBox initStation;
    ComboBox finalStation;

    Button routeButton;

    VBox row4Box;
    //FOURTH ROW
    HBox decorBox;
    Label decorLabel;
    ColorPicker backgroundColorPicker;
    //Label backgroundColorLabel;

    HBox row4ButtonBox;
    Button imageBackgroundButton;
    Button addImageButton;
    Button addLabelButton;
    Button removeElementButton;

    VBox row5Box;
    //FIFTH ROW
    HBox fontLabelBox;
    Label fontLabel;
    ColorPicker fillColorPicker;
    //Label fillColorLabel;

    HBox fontTypeBox;
    Button boldButton;
    Button italicizeButton;
    ComboBox fontSizeChoices;
    ComboBox fontFamilyChoices;

    VBox row6Box;
    //SIXTH ROW

    HBox navBox;
    Label navLabel;
    CheckBox showGrid;

    HBox row6ButtonBox;
    Button zoomInButton;
    Button zoomOutButton;
    Button increaseMapSizeButton;
    Button decreaseMapSizeButton;

    ScrollPane canvasWrapper;
    Pane canvas;

    //Here are the controllers
    CanvasController canvasController;
    MapEditController mapEditController;

    //HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    //FOR DISPLAYING DEBUG STUFF
    Text debugText;

    public mmmWorkspace(AppTemplate initApp) {

        app = initApp;
        gui = app.getGUI();
        initLayout();
        initControllers();
        initStyle();
    }

    public void setDebugText(String text) {
        debugText.setText(text);
    }

    public ColorPicker getFillColorPicker() {
        return fillColorPicker;
    }

    public ColorPicker getLineColorPicker() {
        return lineColorPicker;
    }

    public ColorPicker getBackgroundColorPicker() {
        return backgroundColorPicker;
    }

    public ColorPicker getStationColorPicker() {
        return stationColorPicker;
    }

    public Slider getLineThicknessSlider() {
        return outlineThicknessSlider;
    }

    public Slider getRadiusSlider() {
        return radiusSlider;
    }

    public ComboBox getInitStation() {
        return initStation;
    }

    public ComboBox getFinalStation() {
        return finalStation;
    }

    public ComboBox getMetroLines() {
        return lines;
    }

    public ComboBox getMetroStations() {
        return stations;
    }

    public ComboBox getFontSizes() {
        return fontSizeChoices;
    }

    public ComboBox getFontFamilies() {
        return fontFamilyChoices;
    }

    public Pane getCanvas() {
        return canvas;
    }

    public ScrollPane getCanvasWrapper() {
        return canvasWrapper;
    }

    private void initLayout() {
        editToolbar = new VBox();

        //ROW 1
        row1Box = new VBox();
        row1Box.setSpacing(3);

        lineBox = new HBox();
        lineBox.setSpacing(22.5);
        metroLineLabel = new Label("Metro Lines");
        lines = new ComboBox();
        lines.setPrefWidth(120);
        lineColorPicker = new ColorPicker(Color.BLACK);
        lineColorPicker.setPrefWidth(100);
        lineBox.getChildren().addAll(metroLineLabel, lines, lineColorPicker);

        row1ButtonBox = new HBox();
        row1ButtonBox.setSpacing(6);
        addLineButton = gui.initChildButton(row1ButtonBox, ADDLINE_ICON.toString(), ADDLINE_TOOLTIP.toString(), false);
        removeLineButton = gui.initChildButton(row1ButtonBox, REMOVELINE_ICON.toString(), REMOVELINE_TOOLTIP.toString(), true);
        addStationToLineButton = new Button("Add\nStation");
        addStationToLineButton.setDisable(true);
        removeStationFromLineButton = new Button("Remove\nStation");
        removeStationFromLineButton.setDisable(true);
        row1ButtonBox.getChildren().addAll(addStationToLineButton, removeStationFromLineButton);
        editLineButton = gui.initChildButton(row1ButtonBox, EDIT_LINE_ICON.toString(), EDIT_LINE_TOOLTIP.toString(), true);
        listStationsButton = gui.initChildButton(row1ButtonBox, LIST_ICON.toString(), LIST_TOOLTIP.toString(), true);

        row1SliderBox = new HBox();
        outlineThicknessSlider = new Slider(4, 10, 5);
        row1SliderBox.getChildren().add(outlineThicknessSlider);

        row1Box.getChildren().addAll(lineBox, row1ButtonBox, row1SliderBox);

        //ROW 2
        row2Box = new VBox();
        row2Box.setSpacing(3);

        stationBox = new HBox();
        stationBox.setSpacing(10);
        metroStationLabel = new Label("Metro Stations");
        stations = new ComboBox();
        stations.setPrefWidth(120);
        stationColorPicker = new ColorPicker(Color.WHITE);
        stationColorPicker.setPrefWidth(100);
        stationBox.getChildren().addAll(metroStationLabel, stations, stationColorPicker);

        row2ButtonBox = new HBox();
        row2ButtonBox.setSpacing(5);
        newStationButton = gui.initChildButton(row2ButtonBox, ADDSTATION_ICON.toString(), ADDSTATION_TOOLTIP.toString(), false);
        removeStationButton = gui.initChildButton(row2ButtonBox, REMOVESTATION_ICON.toString(), REMOVESTATION_TOOLTIP.toString(), true);
        snapButton = gui.initChildButton(row2ButtonBox, SNAPGRID_ICON.toString(), SNAPGRID_TOOLTIP.toString(), false);
        moveLabelButton = new Button("Move\nLabel");
        moveLabelButton.setDisable(true);
        row2ButtonBox.getChildren().add(moveLabelButton);
        rotateLabelButton = gui.initChildButton(row2ButtonBox, ROTATE_ICON.toString(), ROTATE_TOOLTIP.toString(), true);

        row2SliderBox = new HBox();
        radiusSlider = new Slider(10, 30, 12);
        row2SliderBox.getChildren().add(radiusSlider);

        row2Box.getChildren().addAll(stationBox, row2ButtonBox, row2SliderBox);

        //ROW 3
        row3Box = new HBox();
        row3Box.setSpacing(10);

        routeBox = new VBox();
        routeBox.setSpacing(5);
        initStation = new ComboBox();
        initStation.setPrefWidth(100);
        initStation.setPromptText("FROM");
        finalStation = new ComboBox();
        finalStation.setPrefWidth(100);
        finalStation.setPromptText("TO");
        routeBox.getChildren().addAll(initStation, finalStation);
        row3Box.getChildren().add(routeBox);
        routeButton = gui.initChildButton(row3Box, ROUTE_ICON.toString(), ROUTE_TOOLTIP.toString(), false);

        //ROW 4
        row4Box = new VBox();
        row4Box.setSpacing(2);

        decorBox = new HBox();
        decorBox.setSpacing(100);
        decorLabel = new Label("Decor");

        backgroundColorPicker = new ColorPicker(Color.WHITE);

        decorBox.getChildren().addAll(decorLabel, backgroundColorPicker);

        row4ButtonBox = new HBox();
        row4ButtonBox.setSpacing(4);
        imageBackgroundButton = new Button("Set Image\nBackground");
        addImageButton = new Button("Add\nImage");
        addLabelButton = new Button("Add\nLabel");
        removeElementButton = new Button("Remove\nElement");
        removeElementButton.setDisable(true);
        row4ButtonBox.getChildren().addAll(imageBackgroundButton, addImageButton, addLabelButton, removeElementButton);

        row4Box.getChildren().addAll(decorBox, row4ButtonBox);

        //ROW 5
        row5Box = new VBox();
        row5Box.setSpacing(2);

        fontLabelBox = new HBox();
        fontLabelBox.setSpacing(227.5);
        fontLabel = new Label("Font");

        fillColorPicker = new ColorPicker(Color.BLACK);
        fillColorPicker.setPrefWidth(100);

        fontLabelBox.getChildren().addAll(fontLabel, fillColorPicker);

        fontTypeBox = new HBox();
        fontTypeBox.setSpacing(7.5);
        boldButton = gui.initChildButton(fontTypeBox, BOLD_ICON.toString(), BOLD_TOOLTIP.toString(), true);
        italicizeButton = gui.initChildButton(fontTypeBox, ITALICIZE_ICON.toString(), ITALICIZE_TOOLTIP.toString(), true);
        fontSizeChoices = new ComboBox<Integer>();
        fontSizeChoices.getItems().addAll(
                10, 11, 12, 14, 16, 18, 20, 22, 24, 28, 32, 36, 40, 46, 52, 58, 64, 72, 80);
        fontSizeChoices.getSelectionModel().selectFirst();

        fontFamilyChoices = new ComboBox<String>();
        fontFamilyChoices.getItems().addAll(
                "Arial",
                "Calibri",
                "Century",
                "Cambria",
                "Courier",
                "Helvetica",
                "Times New Roman",
                "Verdana"
        );
        fontFamilyChoices.getSelectionModel().selectFirst();
        fontTypeBox.getChildren().addAll(fontSizeChoices, fontFamilyChoices);

        row5Box.getChildren().addAll(fontLabelBox, fontTypeBox);

        //ROW 6
        row6Box = new VBox();

        navBox = new HBox();
        navBox.setSpacing(170);
        navLabel = new Label("Navigation");
        showGrid = new CheckBox("Show Grid");
        navBox.getChildren().addAll(navLabel, showGrid);

        row6ButtonBox = new HBox();
        row6ButtonBox.setSpacing(4);
        zoomInButton = gui.initChildButton(row6ButtonBox, ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
        zoomOutButton = gui.initChildButton(row6ButtonBox, ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
        increaseMapSizeButton = gui.initChildButton(row6ButtonBox, MAP_BIG_ICON.toString(), MAP_BIG_TOOLTIP.toString(), false);
        decreaseMapSizeButton = gui.initChildButton(row6ButtonBox, MAP_SMALL_ICON.toString(), MAP_SMALL_TOOLTIP.toString(), false);

        row6Box.getChildren().addAll(navBox, row6ButtonBox);

        editToolbar.setSpacing(6);

        editToolbar.getChildren().addAll(row1Box, row2Box, row3Box, row4Box, row5Box, row6Box);
        canvas = new Pane();
        debugText = new Text();
        canvas.getChildren().add(debugText);
        debugText.setX(100);
        debugText.setY(100);

        mmmData data = (mmmData) app.getDataComponent();
        data.setShapes(canvas.getChildren());

        workspace = new BorderPane();
        editToolbar.setPrefHeight(1500);
        canvasWrapper = new ScrollPane();
        canvasWrapper.setContent(canvas);

        canvasWrapper.setPannable(true);
        ((BorderPane) workspace).setCenter(canvasWrapper);
        canvas.setPrefSize(1500, 900);
        ((BorderPane) workspace).setLeft(editToolbar);
        BackgroundFill fill = new BackgroundFill(Color.WHITE, null, null);
        canvas.setBackground(new Background(fill));
    }

    @Override
    public void reloadWorkspace(AppDataComponent data) {
        mmmData dataManager = (mmmData) data;
        backgroundColorPicker.setValue(dataManager.getBackgroundColor());
        addStationToLineButton.setDisable(dataManager.getStations().size() < 1 && dataManager.getDraggableLines().size() < 1);
    }

    @Override
    public void resetWorkspace() {
        lines.getItems().remove(0, lines.getItems().size());
        stations.getItems().remove(0, stations.getItems().size());

        initStation.getItems().remove(0, initStation.getItems().size());
        finalStation.getItems().remove(0, finalStation.getItems().size());

        lineColorPicker.setValue(Color.BLACK);
        stationColorPicker.setValue(Color.WHITE);
        backgroundColorPicker.setValue(Color.WHITE);
        fillColorPicker.setValue(Color.BLACK);
    }

    private void initControllers() {
        mmmData data = (mmmData) app.getDataComponent();
        mapEditController = new MapEditController(app);

        lines.setOnAction(e -> {
            for (Object shapeToUH : data.getShapes()) {
                data.unhighlightShape((Shape) shapeToUH);
            }
            for (int i = 0; i < data.getDraggableLines().size(); i++) {
                if (data.getDraggableLines().get(i).getName().equals(lines.getSelectionModel().getSelectedItem())) {
                    data.setSelectedLine(data.getDraggableLines().get(i));
                    for (Object shapeToHighlight : data.getDraggableLines().get(i)) {
                        data.highlightShape((Shape) shapeToHighlight);
                    }
                }
            }
        });

        stations.setOnAction(e -> {
            for (Object shapeToUH : data.getShapes()) {
                data.unhighlightShape((Shape) shapeToUH);
            }
            for (int i = 0; i < data.getStations().size(); i++) {
                if (data.getStations().get(i).getName().equals(stations.getSelectionModel().getSelectedItem())) {
                    data.setSelectedShape(((Shape) data.getStations().get(i)));
                    data.highlightShape(data.getSelectedShape());
                    data.highlightShape(((Shape) ((DraggableCircle) data.getSelectedShape()).getStationName()));
                }
            }
        });

        lineColorPicker.setOnAction(e -> {
            if (data.getSelectedLine() != null) {
                data.setLineColor(lineColorPicker.getValue());
            }
        });

        stationColorPicker.setOnAction(e -> {
            if (data.getSelectedShape() != null && data.getSelectedShape() instanceof DraggableCircle) {
                data.setStationColor(stationColorPicker.getValue());
            }
        });

        outlineThicknessSlider.setBlockIncrement(2.5);
        outlineThicknessSlider.valueProperty().addListener(e -> {
            if (data.getSelectedShape() != null && data.getSelectedLine() != null) {
                for (int i = 0; i < data.getSelectedLine().size(); i++) {
                    if (data.getSelectedLine().get(i) instanceof Line) {
                        ((Line) data.getSelectedLine().get(i)).setStrokeWidth(outlineThicknessSlider.getValue());
                    }
                }
            }
        });

        radiusSlider.setBlockIncrement(2);
        radiusSlider.valueProperty().addListener(e -> {
            if (data.getSelectedShape() != null && data.getSelectedShape() instanceof DraggableCircle) {
                ((DraggableCircle) data.getSelectedShape()).setRadius(radiusSlider.getValue());
            }
        });

        fillColorPicker.setOnAction(e -> {
            if (data.getSelectedShape() != null && data.getSelectedShape() instanceof DraggableText) {
                mapEditController.processSelectFillColor();
            }
        });

        fontFamilyChoices.setOnAction(e -> {
            if (data.getSelectedShape() != null && data.getSelectedShape() instanceof DraggableText) {
                DraggableText tempText = (DraggableText) data.getSelectedShape();
                FontWeight b;
                FontPosture i;
                if (tempText.getFont().getName().contains("Bold")) {
                    b = FontWeight.BOLD;
                } else {
                    b = FontWeight.NORMAL;
                }
                if (tempText.getFont().getName().contains("Italic")) {
                    i = FontPosture.ITALIC;
                } else {
                    i = FontPosture.REGULAR;
                }
                ((DraggableText) data.getSelectedShape()).setFont(Font.font((String) fontFamilyChoices.getSelectionModel().getSelectedItem(), b, i, (int) (fontSizeChoices.getSelectionModel().getSelectedItem())));
            }
        });

        fontSizeChoices.setOnAction(e -> {
            if (data.getSelectedShape() != null && data.getSelectedShape() instanceof DraggableText) {
                if (data.getSelectedShape() != null && data.getSelectedShape() instanceof DraggableText) {
                    DraggableText tempText = (DraggableText) data.getSelectedShape();
                    FontWeight b;
                    FontPosture i;
                    if (tempText.getFont().getName().contains("Bold")) {
                        b = FontWeight.BOLD;
                    } else {
                        b = FontWeight.NORMAL;
                    }
                    if (tempText.getFont().getName().contains("Italic")) {
                        i = FontPosture.ITALIC;
                    } else {
                        i = FontPosture.REGULAR;
                    }
                    ((DraggableText) data.getSelectedShape()).setFont(Font.font((String) fontFamilyChoices.getSelectionModel().getSelectedItem(), b, i, (int) (fontSizeChoices.getSelectionModel().getSelectedItem())));
                }
            }
        });

        addLineButton.setOnAction(e -> {
            mapEditController.processAddLine();
        });

        removeLineButton.setOnAction(e -> {
            if (data.getSelectedLine() != null) {
                data.getDraggableLines().remove(data.getSelectedLine());
                for (int i = 0; i < data.getSelectedLine().size(); i++) {
                    lines.getItems().remove(((DraggableText) data.getSelectedLine().getFirst()).getText());

                    if (data.getSelectedLine().get(i) instanceof Line || data.getSelectedLine().get(i) instanceof DraggableText) {
                        System.out.println("Removed");
                        data.getShapes().remove(data.getShapes().indexOf(data.getSelectedLine().get(i)));
                    } else if (data.getSelectedLine().get(i) instanceof DraggableCircle) {
                        ((DraggableCircle) data.getSelectedLine().get(i)).setLineParent(null);
                    }
                }
                data.setSelectedLine(null);
            }
        });

        addStationToLineButton.setOnAction(e -> {
            mapEditController.processAddStationToLine();
        });

        removeStationFromLineButton.setOnAction(e -> {
            mapEditController.processRemoveStationFromLine();
        });

        backgroundColorPicker.setOnAction(e -> {
            mapEditController.processSelectBackgroundColor();
        });

        newStationButton.setOnAction(e -> {
            mapEditController.processNewStation();
        });

        removeStationButton.setOnAction(e -> {
            if (data.getSelectedShape() instanceof DraggableCircle) {
                if (((DraggableCircle) data.getSelectedShape()).getLineParent() != null) {
                    getMetroLines().getItems().add(((DraggableText) (((DraggableCircle) data.getSelectedShape()).getLineParent().get(0))).getText());
                    int start = data.getShapes().indexOf((((DraggableCircle) data.getSelectedShape()).getLineParent().getStartPoint()));
                    int end = data.getShapes().indexOf((((DraggableCircle) data.getSelectedShape()).getLineParent().getEndPoint()));
                    Paint testColor = ((Line) ((DraggableCircle) data.getSelectedShape()).getLineParent().get(1)).getStroke();
                    double testWidth = ((Line) ((DraggableCircle) data.getSelectedShape()).getLineParent().get(1)).getStrokeWidth();

                    data.getShapes().remove(start, end + 1);

                    DraggableLineWrapper lineToRemoveFrom = ((DraggableCircle) data.getSelectedShape()).getLineParent().removeStationFromLine((DraggableCircle) data.getSelectedShape());

                    for (int i = 0; i < lineToRemoveFrom.size(); i++) {
                        if (lineToRemoveFrom.get(i) != null) {
                            Shape newShape = (Shape) lineToRemoveFrom.get(i);
                            data.getShapes().add(newShape);
                            if (newShape instanceof DraggableCircle) {
                                data.getShapes().add(((DraggableCircle) newShape).getStationName());
                            } else if (newShape instanceof Line) {
                                ((Line) newShape).setStroke(testColor);
                                ((Line) newShape).setStrokeWidth(testWidth);
                            }
                        }
                    }
                    getMetroStations().getItems().remove(((DraggableCircle) data.getSelectedShape()).getStationName().getText());
                    data.removeShape(((DraggableCircle) data.getSelectedShape()).getStationName());
                    data.getStations().remove((DraggableCircle) data.getSelectedShape());

                    data.removeSelectedShape();
                    removeStationButton.setDisable(true);
                } else {

                    getMetroStations().getItems().remove(((DraggableCircle) data.getSelectedShape()).getStationName().getText());
                    data.removeShape(((DraggableCircle) data.getSelectedShape()).getStationName());

                    int index = data.getStations().indexOf(data.getSelectedShape());

                    //REMOVE FROM COMBOBOX, SHAPES LIST, AND STATIONS LIST
                    data.getStations().remove(index);
                    data.removeSelectedShape();
                    removeStationButton.setDisable(true);
                }
            }
        });

        editLineButton.setOnAction(e -> {
            mapEditController.processEditLine();
        });

        listStationsButton.setOnAction(e -> {
            mapEditController.processListStations();
        });

        snapButton.setOnAction(e -> {
            mapEditController.snapGrid();
        });

        moveLabelButton.setOnAction(e -> {
            mapEditController.processMoveLabel();
        });

        rotateLabelButton.setOnAction(e -> {
            mapEditController.processRotateLabel();
        });

        routeButton.setOnAction(e -> {
        });

        imageBackgroundButton.setOnAction(e -> {
            mapEditController.processImageBackground();
        });

        addImageButton.setOnAction(e -> {
            mapEditController.processAddImage();
        });

        addLabelButton.setOnAction(e -> {
            mapEditController.processAddText();
        });

        removeElementButton.setOnAction(e -> {
            mapEditController.processRemoveElement();
        });

        boldButton.setOnAction(e -> {
            mapEditController.processBold();
        });

        italicizeButton.setOnAction(e -> {
            mapEditController.processItalicize();
        });

        zoomInButton.setOnAction(e -> {
            mapEditController.zoomIn();
        });

        zoomOutButton.setOnAction(e -> {
            mapEditController.zoomOut();
        });

        increaseMapSizeButton.setOnAction(e -> {
            canvas.setPrefSize(canvas.getWidth() * 1.1, canvas.getHeight() * 1.1);
        });

        decreaseMapSizeButton.setOnAction(e -> {
            canvas.setPrefSize(canvas.getWidth() / 1.1, canvas.getHeight() / 1.1);
        });

        showGrid.setOnAction(e -> {
            mapEditController.showGrid();
        });

        app.getGUI().getAboutButton().setOnAction(e -> {
            mapEditController.processAbout();
        });

        app.getGUI().getUndoButton().setOnAction(e -> {
            data.getJTPS().undoTransaction();
        });

        app.getGUI().getRedoButton().setOnAction(e -> {
            data.getJTPS().doTransaction();
        });

        app.getGUI().getExportButton().setOnAction(e -> {
            if (!(app.getGUI().getFileController().isSaved())) {
                app.getGUI().getFileController().handleSaveRequest();
            }
            mapEditController.processSnapshot();
            //int index = app.getGUI().getFileController().filePath().lastIndexOf(".");
            String exportFilePath = app.getGUI().getFileController().filePath()
                    + " Metro.json";
            File exportTo = new File(exportFilePath);
            try {
                app.getFileComponent().exportData(data, exportTo.getPath());
            } catch (Exception export) {
                System.out.println("Error exporting work");
            }
        });

        canvasController = new CanvasController(app);

        canvasWrapper.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.W) {
                canvas.setTranslateY(canvas.getTranslateY() - 0.1 * canvas.getHeight());
            } else if (e.getCode() == KeyCode.A) {
                canvas.setTranslateX(canvas.getTranslateX() - 0.1 * canvas.getWidth());
            } else if (e.getCode() == KeyCode.S) {
                canvas.setTranslateY(canvas.getTranslateY() + 0.1 * canvas.getHeight());
            } else if (e.getCode() == KeyCode.D) {
                canvas.setTranslateX(canvas.getTranslateX() + 0.1 * canvas.getWidth());
            }
        });

        canvas.setOnMousePressed(e -> {
            int x = e.getClickCount();
            canvasController.processCanvasMousePress((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseReleased(e -> {
            canvasController.processCanvasMouseRelease((int) e.getX(), (int) e.getY());
        });
        canvas.setOnMouseDragged(e -> {
            canvasController.processCanvasMouseDragged((int) e.getX(), (int) e.getY());
        });
    }

    private void initStyle() {

        canvas.getStyleClass().add(CLASS_RENDER_CANVAS);

        fillColorPicker.getStyleClass().add(CLASS_BUTTON);
        lineColorPicker.getStyleClass().add(CLASS_BUTTON);
        stationColorPicker.getStyleClass().add(CLASS_BUTTON);
        backgroundColorPicker.getStyleClass().add(CLASS_BUTTON);

        editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
        row1Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row2Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row3Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row4Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row6Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);

        metroLineLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        metroStationLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        decorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        fontLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
        navLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);

    }

    public void updateButtons() {
        mmmData dataManager = (mmmData) app.getDataComponent();
        
        app.getGUI().getRedoButton().setDisable(dataManager.getJTPS().redoDisabled());
        app.getGUI().getUndoButton().setDisable(dataManager.getJTPS().undoDisabled());
        
        addStationToLineButton.setDisable(dataManager.getDraggableLines().isEmpty() || dataManager.getStations().isEmpty());
        listStationsButton.setDisable(dataManager.getSelectedLine() == null);
        editLineButton.setDisable(dataManager.getSelectedLine() == null);
        removeStationFromLineButton.setDisable(dataManager.getSelectedLine() != null && dataManager.getSelectedLine().size() < 5);
        if(dataManager.getSelectedShape() instanceof DraggableCircle) {
            
            moveLabelButton.setDisable(false);
            removeStationButton.setDisable(false);
            rotateLabelButton.setDisable(false);
            moveLabelButton.setDisable(false);
            snapButton.setDisable(false);
            if(((DraggableCircle)dataManager.getSelectedShape()).getLineParent() != null)
                removeStationFromLineButton.setDisable(false);
            
        } else if(dataManager.getSelectedShape() instanceof DraggableText) {
            moveLabelButton.setDisable(false);
            removeElementButton.setDisable(false);
            rotateLabelButton.setDisable(false);
            boldButton.setDisable(false);
            italicizeButton.setDisable(false);
        } else if(dataManager.getSelectedShape() instanceof DraggableImage) {
            snapButton.setDisable(false);
            removeElementButton.setDisable(false);
        } else if(dataManager.getSelectedShape() == null) {
            moveLabelButton.setDisable(true);
            removeStationButton.setDisable(true);
            rotateLabelButton.setDisable(true);
            moveLabelButton.setDisable(true);
            snapButton.setDisable(true);
            snapButton.setDisable(true);
            removeElementButton.setDisable(true);
            boldButton.setDisable(true);
            italicizeButton.setDisable(true);
        }
        
        
    }

}
