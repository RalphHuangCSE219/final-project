/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import java.io.File;
import java.io.IOException;
import javafx.beans.property.DoubleProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.transform.Rotate;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import mmm.data.BackgroundColorTransaction;
import mmm.data.DraggableCircle;
import mmm.data.DraggableImage;
import mmm.data.DraggableText;
import mmm.data.FillColorTransaction;
import mmm.data.LineColorTransaction;
import mmm.data.StationColorTransaction;
import mmm.data.mmmData;
import mmm.data.mmmState;

/**
 *
 * @author ralph
 */
public class MapEditController {

    AppTemplate app;
    mmmData dataManager;

    public MapEditController(AppTemplate initApp) {
        app = initApp;
        dataManager = (mmmData) app.getDataComponent();
    }

    public void processSelectSelectionTool() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.DEFAULT);

        dataManager.setState(mmmState.SELECTING_SHAPE);

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveSelectedShape() {
        dataManager.removeSelectedShape();

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
        app.getGUI().updateToolbarControls(false);
    }

    public void processSelectFillColor() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getFillColorPicker().getValue();

        if (selectedColor != null) {
            FillColorTransaction fct = new FillColorTransaction((Color)dataManager.getSelectedShape().getFill(), selectedColor, dataManager, dataManager.getSelectedShape());
            dataManager.getJTPS().addTransaction(fct);
            dataManager.setCurrentFillColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processSelectLineColor() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getLineColorPicker().getValue();
        if (selectedColor != null) {
            LineColorTransaction lct = new LineColorTransaction(dataManager.getSelectedLine().getColor(), selectedColor, dataManager, dataManager.getSelectedLine());
            dataManager.getJTPS().addTransaction(lct);
            dataManager.setLineColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processSelectBackgroundColor() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getBackgroundColorPicker().getValue();
        if (selectedColor != null) {
            BackgroundColorTransaction bct = new BackgroundColorTransaction(dataManager.getBackgroundColor(), selectedColor, dataManager);
            dataManager.getJTPS().addTransaction(bct);
            dataManager.setBackgroundColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processSelectStationColor() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Color selectedColor = workspace.getStationColorPicker().getValue();
        if (selectedColor != null) {
            StationColorTransaction fct = new StationColorTransaction((Color) ((DraggableCircle)dataManager.getSelectedShape()).getFill(), selectedColor, dataManager, dataManager.getSelectedShape());
            dataManager.getJTPS().addTransaction(fct);
            dataManager.setStationColor(selectedColor);
            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processAddLine() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mmmState.STARTING_LINE);

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processNewStation() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mmmState.STARTING_STATION);

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processAddStationToLine() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);

        dataManager.setState(mmmState.ADDING_STATION_TO_LINE);

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }

    public void processRemoveStationFromLine() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        dataManager.setState(mmmState.REMOVING_STATION_FROM_LINE);

        workspace.reloadWorkspace(dataManager);
    }

    public void processAddText() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        dataManager.setState(mmmState.STARTING_TEXT);

        workspace.reloadWorkspace(dataManager);
    }

    public void processAddImage() {
        Scene scene = app.getGUI().getPrimaryScene();
        scene.setCursor(Cursor.CROSSHAIR);
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        dataManager.setState(mmmState.STARTING_IMAGE);

        workspace.reloadWorkspace(dataManager);
    }

    public void processEditLine() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        Dialog editLineDialog = new Dialog<>();
        editLineDialog.setTitle("Edit Line");
        editLineDialog.setHeaderText(null);
        editLineDialog.setResizable(false);
        editLineDialog.initStyle(StageStyle.UTILITY);

        VBox editLineBox = new VBox(5);
        TextField newName = new TextField();
        newName.setPromptText("Edit name here");
        ColorPicker newColorPicker = new ColorPicker(dataManager.getSelectedLine().getColor());
        editLineBox.getChildren().addAll(newName, newColorPicker);

        editLineDialog.getDialogPane().setContent(editLineBox);
        editLineDialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        Button ok = (Button) editLineDialog.getDialogPane().lookupButton(ButtonType.OK);
        ok.addEventFilter(ActionEvent.ACTION, e -> {
            workspace.getMetroLines().getItems().add(newName.getText());
            workspace.getMetroLines().getItems().remove(dataManager.getSelectedLine().getName());
            workspace.getLineColorPicker().setValue(newColorPicker.getValue());
            workspace.getMetroLines().setValue(newName.getText());
            dataManager.getSelectedLine().setColor(newColorPicker.getValue());
            dataManager.getSelectedLine().setName(newName.getText());
            
        });

        editLineDialog.showAndWait();
        
        workspace.reloadWorkspace(dataManager);
    }

    public void processListStations() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        String lineName = dataManager.getSelectedLine().getName();
        if (lineName == null) {
            lineName = dataManager.getSelectedLine().name;
        }
        if (lineName == null) {
            lineName = "Line Name Error";
        }

        String stops = "";
        for (int i = 0; i < dataManager.getSelectedLine().getStations().size(); i++) {
            stops += ("\u2022 " + dataManager.getSelectedLine().getStations().get(i).getName() + "\n");
        }

        Alert listStations = new Alert(AlertType.INFORMATION);
        listStations.setHeaderText(lineName + " Line Stops");
        listStations.setTitle("Metro Map Maker - Line Stops");
        listStations.initStyle(StageStyle.UTILITY);
        listStations.setContentText(stops);
        listStations.showAndWait();

        workspace.reloadWorkspace(dataManager);
    }
    
    public void processRotateLabel() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        
        if(dataManager.getSelectedShape() != null && dataManager.getSelectedShape() instanceof DraggableText) {
            if(((DraggableText)dataManager.getSelectedShape()).getRotate() == 90)
                ((DraggableText)dataManager.getSelectedShape()).setRotate(0);
            else
                ((DraggableText)dataManager.getSelectedShape()).setRotate(90);
        }
    }
    
    public void processMoveLabel() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        if(dataManager.getSelectedShape() != null && dataManager.getSelectedShape() instanceof DraggableText) {
            double x = ((DraggableText) dataManager.getSelectedShape()).getStationParent().getCenterX();
            double y = ((DraggableText) dataManager.getSelectedShape()).getStationParent().getCenterY();
            ((DraggableText) dataManager.getSelectedShape()).getTransforms().add(new Rotate(90, x, y));
        } else if(dataManager.getSelectedShape() != null && dataManager.getSelectedShape() instanceof DraggableCircle) {
            double x = ((DraggableCircle)dataManager.getSelectedShape()).getCenterX();
            double y = ((DraggableCircle)dataManager.getSelectedShape()).getCenterY();
            ((DraggableText) ((DraggableCircle)dataManager.getSelectedShape()).getStationName()).getTransforms().add(new Rotate(90, x, y));
        }
        
    }
    
    public void processRemoveElement() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        if(dataManager.getSelectedShape() != null) {
            if(dataManager.getSelectedShape() instanceof DraggableText || dataManager.getSelectedShape() instanceof DraggableImage) {
                dataManager.getShapes().remove(dataManager.getSelectedShape());
                dataManager.setSelectedShape(null);
            }
        }
        workspace.reloadWorkspace(dataManager);
    }
    
    public void processAbout() {
        Alert about = new Alert(AlertType.INFORMATION);
        about.setTitle("About");
        about.setHeaderText("About Our Application");
        about.setContentText("Metro Map Maker\n"
                + "Frameworks used:\n"
                + "\tDesktopJavaFramework\n"
                + "\tjTPS\n"
                + "\tPropertiesManager\n"
                + "Developers:\n"
                + "\tRalph Huang");
        about.initStyle(StageStyle.UTILITY);
        about.showAndWait();
    }
    
    public void processImageBackground() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose Image File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(app.getGUI().getWindow());
        if (selectedFile != null) {
            Image selectedImage = new Image("file:///" + selectedFile.getAbsoluteFile());
            ImagePattern image = new ImagePattern(selectedImage);
            BackgroundFill fill = new BackgroundFill(image , null, null);
            Background bg = new Background(fill);
            workspace.getCanvas().setBackground(bg);
        }
    }
    
    public void snapGrid() {
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        if(dataManager.getSelectedShape() != null) {
            double roundedx;
            double roundedy;
            double origx;
            double origy;
            if(dataManager.getSelectedShape() instanceof DraggableCircle) {
                origx = ((DraggableCircle)dataManager.getSelectedShape()).getCenterX();
                origy = ((DraggableCircle)dataManager.getSelectedShape()).getCenterY();
                if(origx%100 < 50)
                    roundedx = origx - (origx%100);
                else if (origx%100 > 50)
                    roundedx = origx + (100 - (origx%100));
                else if (origx%100 == 50)
                    roundedx = origx + 50;
                else
                    roundedx = origx;
                
                if(origy%100 < 50)
                    roundedy = origy - (origy%100);
                else if (origy%100 > 50)
                    roundedy = origy + (100 - (origy%100));
                else if (origy%100 == 50)
                    roundedy = origy + 50;
                else
                    roundedy = origy;
                
                ((DraggableCircle)dataManager.getSelectedShape()).drag((int)roundedx, (int)roundedy);
            } else if(dataManager.getSelectedShape() instanceof DraggableText) {
                origx = ((DraggableText) dataManager.getSelectedShape()).getX();
                origy = ((DraggableText) dataManager.getSelectedShape()).getY();
                if (origx % 100 < 50)
                    roundedx = origx - (origx % 100);
                else if (origx % 100 > 50)
                    roundedx = origx + (100 - (origx % 100));
                else if (origx % 100 == 50)
                    roundedx = origx + 50;
                else
                    roundedx = origx;
                
                if(origy%100 < 50)
                    roundedy = origy - (origy%100);
                else if (origy%100 > 50)
                    roundedy = origy + (100 - (origy%100));
                else if (origy%100 == 50)
                    roundedy = origy + 50;
                else
                    roundedy = origy;
                
                ((DraggableText)dataManager.getSelectedShape()).drag((int)roundedx, (int)roundedy);
            } else if(dataManager.getSelectedShape() instanceof DraggableImage) {
                origx = ((DraggableImage) dataManager.getSelectedShape()).getX();
                origy = ((DraggableImage) dataManager.getSelectedShape()).getY();
                if (origx % 100 < 50)
                    roundedx = origx - (origx % 100);
                else if (origx % 100 > 50)
                    roundedx = origx + (100 - (origx % 100));
                else if (origx % 100 == 50)
                    roundedx = origx + 50;
                else
                    roundedx = origx;
                
                if(origy%100 < 50)
                    roundedy = origy - (origy%100);
                else if (origy%100 > 50)
                    roundedy = origy + (100 - (origy%100));
                else if (origy%100 == 50)
                    roundedy = origy + 50;
                else
                    roundedy = origy;
                
                ((DraggableImage)dataManager.getSelectedShape()).drag((int)roundedx, (int)roundedy);
            } 
        }
    }
    
    public void processBold() {
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        if(dataManager.getSelectedShape() != null && dataManager.getSelectedShape() instanceof DraggableText) {
            DraggableText originalSelected = (DraggableText) dataManager.getSelectedShape();
            
            String fontFamily = (String) workspace.getFontFamilies().getSelectionModel().getSelectedItem();      
            
            if (originalSelected.getFont().getName().contains("Bold") && originalSelected.getFont().getName().contains("Italic")) {                      
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.NORMAL, FontPosture.ITALIC, originalSelected.getHeight()));
            } else if(originalSelected.getFont().getName().contains("Bold")) {
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.NORMAL, FontPosture.REGULAR, originalSelected.getHeight()));
            } else if(originalSelected.getFont().getName().contains("Italic")) {
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.ITALIC, originalSelected.getHeight()));
            } else {
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.REGULAR, originalSelected.getHeight()));
            }
        }
        workspace.reloadWorkspace(dataManager);
    }
    
    public void processItalicize() {
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        if(dataManager.getSelectedShape() != null && dataManager.getSelectedShape() instanceof DraggableText) {
            DraggableText originalSelected = (DraggableText) dataManager.getSelectedShape();
            
            String fontFamily = (String) workspace.getFontFamilies().getSelectionModel().getSelectedItem();      
            
            if (originalSelected.getFont().getName().contains("Bold") && originalSelected.getFont().getName().contains("Italic")) {                      
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.REGULAR, originalSelected.getHeight()));
            } else if(originalSelected.getFont().getName().contains("Bold")) {
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.ITALIC, originalSelected.getHeight()));
            } else if(originalSelected.getFont().getName().contains("Italic")) {
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.NORMAL, FontPosture.REGULAR, originalSelected.getHeight()));
            } else {
                ((DraggableText)dataManager.getSelectedShape()).setFont(Font.font(fontFamily, FontWeight.NORMAL, FontPosture.ITALIC, originalSelected.getHeight()));
            }
        }
        workspace.reloadWorkspace(dataManager);
    }
    
    public void zoomIn() {
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        DoubleProperty xpro = workspace.getCanvas().scaleXProperty();
        DoubleProperty ypro = workspace.getCanvas().scaleYProperty();
        double xprop = xpro.doubleValue();
        double yprop = ypro.doubleValue();
        workspace.getCanvas().setScaleX(.1 + xprop);
        workspace.getCanvas().setScaleY(.1 + yprop);
    }
    
    public void zoomOut() {
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        DoubleProperty xpro = workspace.getCanvas().scaleXProperty();
        DoubleProperty ypro = workspace.getCanvas().scaleYProperty();
        double xprop = xpro.doubleValue();
        double yprop = ypro.doubleValue();
        workspace.getCanvas().setScaleX(xprop - .1);
        workspace.getCanvas().setScaleY(yprop - .1);
    }
    
    public void showGrid() {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Background original = workspace.getCanvas().getBackground();
        
        if(workspace.getCanvas().getStyleClass().contains("canvas_grid"))
            workspace.getCanvas().getStyleClass().remove("canvas_grid");
        else
            workspace.getCanvas().getStyleClass().add("canvas_grid");
    }
    
    public void processSnapshot() {
	mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        String exportFilePath = app.getGUI().getFileController().filePath()
                    + " Metro.png";
	File file = new File(exportFilePath);
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	}
    }
}
