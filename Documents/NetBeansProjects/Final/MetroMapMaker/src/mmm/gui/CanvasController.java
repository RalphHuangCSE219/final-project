/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.gui;

import djf.AppTemplate;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import mmm.data.Draggable;
import mmm.data.DraggableCircle;
import mmm.data.DraggableImage;
import mmm.data.DraggableLine;
import mmm.data.DraggableLineWrapper;
import mmm.data.DraggableText;
import mmm.data.mmmData;
import mmm.data.mmmState;

/**
 *
 * @author ralph
 */
public class CanvasController {

    AppTemplate app;

    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }

    public void processCanvasMousePress(int x, int y) {
        mmmData dataManager = (mmmData) app.getDataComponent();
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        if (dataManager.isInState(mmmState.SELECTING_SHAPE)) {
            Shape shape = dataManager.selectTopShape(x, y);
            Scene scene = app.getGUI().getPrimaryScene();
            workspace.snapButton.setDisable(shape == null);
            workspace.removeLineButton.setDisable(dataManager.getSelectedLine() == null);

            if (shape != null) {

                if (!(shape instanceof Line)) {
                    scene.setCursor(Cursor.MOVE);
                    dataManager.setState(mmmState.DRAGGING_SHAPE);
                }
                app.getGUI().updateToolbarControls(false);
                if (dataManager.getSelectedLine() != null) {

                    workspace.updateButtons();
                    workspace.getMetroLines().getSelectionModel().select(dataManager.getSelectedLine().getName());
                    workspace.getLineColorPicker().setValue(dataManager.getSelectedLine().getColor());
                    workspace.getLineThicknessSlider().setValue(dataManager.getSelectedLine().getWidth());

                }
                if (shape instanceof DraggableCircle) {
                    workspace.updateButtons();
                    
                    workspace.getMetroStations().getSelectionModel().select(((DraggableCircle) dataManager.getSelectedShape()).getName());
                    //NEED TO CHANGE VALUE OF COLOR PICKER
                    String colorx = ((DraggableCircle) dataManager.getSelectedShape()).getFill().toString();
                    workspace.getStationColorPicker().setValue(Color.valueOf(colorx));
                }
                if (shape instanceof DraggableText) {
                    workspace.updateButtons();
                    workspace.getFontFamilies().getSelectionModel().select(((DraggableText) shape).getFont().getFamily());
                    System.out.println((int) ((DraggableText) shape).getFont().getSize());
                    workspace.getFontSizes().getSelectionModel().select((int) ((DraggableText) shape).getFont().getSize());
                    String colorx = ((DraggableText) dataManager.getSelectedShape()).getFill().toString();
                    workspace.getFillColorPicker().setValue(Color.valueOf(colorx));
                }

            } else {
                workspace.updateButtons();
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(mmmState.DRAGGING_NOTHING);
                dataManager.setSelectedLine(null);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);

                //BUTTONS TO DISaBLE
            }

        } else if (dataManager.isInState(mmmState.STARTING_LINE)) {
            dataManager.startNewLine(x, y);

        } else if (dataManager.isInState(mmmState.STARTING_STATION)) {
            dataManager.startNewStation(x, y);

        } else if (dataManager.isInState(mmmState.ADDING_STATION_TO_LINE)) {

            Shape testShape = dataManager.selectTopShape(x, y);
            DraggableCircle stationToAdd;
            if (testShape instanceof DraggableCircle) {
                stationToAdd = (DraggableCircle) testShape;
                //IF THERE IS A SELECTED LINE, ADD TO IT
                if (dataManager.getSelectedLine() != null) {
                    Color testColor = dataManager.getSelectedLine().getColor();
                    Paint testColor1 = ((Line) dataManager.getSelectedLine().get(1)).getStroke();
                    double testWidth1 = ((Line) dataManager.getSelectedLine().get(1)).getStrokeWidth();
                    if (stationToAdd instanceof DraggableCircle) {
                        DraggableLineWrapper tempLine = dataManager.getSelectedLine();
                        dataManager.getDraggableLines().remove(dataManager.getSelectedLine());

                        int start = dataManager.getShapes().indexOf(dataManager.getSelectedLine().getStartPoint());
                        int end = dataManager.getShapes().indexOf(dataManager.getSelectedLine().getEndPoint());
                        dataManager.getShapes().remove(start, end + 1);
                        dataManager.getShapes().removeAll(((DraggableCircle) (dataManager.getSelectedShape())).getStationName(), dataManager.getSelectedShape());

                        tempLine = tempLine.addStationToLine(stationToAdd);
                        tempLine.setColor(testColor);
                        tempLine.setWidth(testWidth1);
                        for (Object line : tempLine) {
                            if (line instanceof Line) {
                                ((Line) line).setStroke(testColor1);
                                ((Line) line).setStrokeWidth(testWidth1);
                            }
                        }
                        for (int i = 0; i < tempLine.size(); i++) {
                            if (tempLine.get(i) != null) {
                                Shape newShape = (Shape) tempLine.get(i);
                                dataManager.getShapes().add(newShape);
                                if (newShape instanceof DraggableCircle) {
                                    dataManager.getShapes().add(((DraggableCircle) newShape).getStationName());
                                }
                            }
                        }
                        dataManager.getDraggableLines().add(tempLine);
                        dataManager.setSelectedLine(tempLine);
                        workspace.getInitStation().getItems().add(((DraggableCircle) stationToAdd).getName());
                        workspace.getFinalStation().getItems().add(((DraggableCircle) stationToAdd).getName());
                    } else {
                        dataManager.setState(mmmState.SELECTING_SHAPE);
                    }
                } else {  //CHOOSE THE LINE TO ADD TO

                    Dialog stationToLineDialog = new Dialog<>();
                    stationToLineDialog.setTitle("Select Line To Add To");
                    stationToLineDialog.setHeaderText(null);
                    stationToLineDialog.setResizable(false);
                    ComboBox selectLine = workspace.getMetroLines();
                    stationToLineDialog.getDialogPane().setContent(selectLine);
                    stationToLineDialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
                    stationToLineDialog.showAndWait();

                    DraggableLineWrapper lineToAddTo;
                    for (Object find : dataManager.getDraggableLines()) {
                        if ((((DraggableLineWrapper) find).startPoint).getText().equals(selectLine.getValue())) {
                            dataManager.setSelectedLine((DraggableLineWrapper) find);
                            Color testColor = dataManager.getSelectedLine().getColor();
                            Paint testColor1 = ((Line) dataManager.getSelectedLine().get(1)).getStroke();
                            double testWidth1 = ((Line) dataManager.getSelectedLine().get(1)).getStrokeWidth();
                            lineToAddTo = (DraggableLineWrapper) find;

                            //NEED TO REMOVE ORIGINAL LINE AND RECREATE IT
                            int start = dataManager.getShapes().indexOf(dataManager.getSelectedLine().getStartPoint());
                            int end = dataManager.getShapes().indexOf(dataManager.getSelectedLine().getEndPoint());
                            dataManager.getShapes().remove(start, end + 1);
                            dataManager.getShapes().removeAll(((DraggableCircle) (dataManager.getSelectedShape())).getStationName(), dataManager.getSelectedShape());

                            lineToAddTo = lineToAddTo.addStationToLine(stationToAdd);
                            for (Object line : lineToAddTo) {
                                if (line instanceof Line) {
                                    ((Line) line).setStroke(testColor1);
                                    ((Line) line).setStrokeWidth(testWidth1);
                                }
                            }
                            for (int i = 0; i < lineToAddTo.size(); i++) {
                                if (lineToAddTo.get(i) != null) {
                                    Shape newShape = (Shape) lineToAddTo.get(i);
                                    dataManager.getShapes().add(newShape);
                                    if (newShape instanceof DraggableCircle) {
                                        dataManager.getShapes().add(((DraggableCircle) newShape).getStationName());
                                    }
                                }
                            }
                            dataManager.getDraggableLines().remove(dataManager.getSelectedLine());
                            dataManager.getDraggableLines().add(lineToAddTo);
                            dataManager.setSelectedLine(lineToAddTo);
                            lineToAddTo.setColor(testColor);
                            break;
                        }
                    }

                    dataManager.setState(mmmState.SELECTING_SHAPE);
                    workspace.getInitStation().getItems().add(((DraggableCircle) stationToAdd).getName());
                    workspace.getFinalStation().getItems().add(((DraggableCircle) stationToAdd).getName());
                }
            } else {
                dataManager.setState(mmmState.SELECTING_SHAPE);
            }

        } else if (dataManager.isInState(mmmState.REMOVING_STATION_FROM_LINE)) {
            if (dataManager.getSelectedLine() != null) {
                if (dataManager.selectTopShape(x, y) instanceof DraggableCircle) {
                    DraggableCircle stationToRemove = (DraggableCircle) dataManager.selectTopShape(x, y);
                    int start = dataManager.getShapes().indexOf(dataManager.getSelectedLine().getStartPoint());
                    int end = dataManager.getShapes().indexOf(dataManager.getSelectedLine().getEndPoint());
                    Paint testColor = ((Line) dataManager.getSelectedLine().get(1)).getStroke();
                    double testWidth = ((Line) dataManager.getSelectedLine().get(1)).getStrokeWidth();

                    dataManager.getShapes().remove(start, end + 1);
                    DraggableLineWrapper lineToRemoveFrom = dataManager.getSelectedLine().removeStationFromLine(stationToRemove);
                    dataManager.setSelectedLine(lineToRemoveFrom);
                    for (int i = 0; i < lineToRemoveFrom.size(); i++) {
                        if (lineToRemoveFrom.get(i) != null) {
                            Shape newShape = (Shape) lineToRemoveFrom.get(i);
                            dataManager.getShapes().add(newShape);
                            if (newShape instanceof DraggableCircle) {
                                dataManager.getShapes().add(((DraggableCircle) newShape).getStationName());
                            } else if (newShape instanceof Line) {
                                ((Line) newShape).setStroke(testColor);
                                ((Line) newShape).setStrokeWidth(testWidth);
                            }
                        }
                    }
                    stationToRemove.setCenterX(stationToRemove.getCenterX() + 10);
                    stationToRemove.setCenterY(stationToRemove.getCenterY() + 10);
                    dataManager.getShapes().add(stationToRemove);
                    dataManager.getShapes().add(stationToRemove.getStationName());
                }
                dataManager.setState(mmmState.SELECTING_SHAPE);
            }
        } else if (dataManager.isInState(mmmState.STARTING_IMAGE)) {
            dataManager.startNewImage(x, y);
        } else if (dataManager.isInState(mmmState.STARTING_TEXT)) {
            dataManager.startNewText(x, y);
        }
        workspace.reloadWorkspace(dataManager);
    }

    public void processCanvasMouseDragged(int x, int y) {
        mmmData dataManager = (mmmData) app.getDataComponent();
        if (dataManager.isInState(mmmState.DRAGGING_SHAPE)) {
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            try {
                selectedDraggableShape.drag(x, y);
            } catch (NullPointerException e) {

            }
            app.getGUI().updateToolbarControls(false);
        }
    }

    public void processCanvasMouseRelease(int x, int y) {
        mmmData dataManager = (mmmData) app.getDataComponent();
        if (dataManager.isInState(mmmState.DRAGGING_SHAPE)) {
            dataManager.setState(mmmState.SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(mmmState.DRAGGING_NOTHING)) {
            dataManager.setState(mmmState.SELECTING_SHAPE);
        }
    }

}
