/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.util.LinkedList;
import javafx.scene.shape.Line;

/**
 *
 * @author ralph
 */
public class DraggableLine extends Line implements Draggable {
    double startX;
    double startY;
    double endX;
    double endY;
    LinkedList<DraggableCircle> stations;
    String name;
    DraggableText startPoint;
    DraggableText endPoint;
    
    
    public void setName(String s) {
        name = s;
        startPoint.setText(name);
        endPoint.setText(name);
    }
    
    public String getName() {
        return name;
    }
    
    public mmmState getStartingState() {
        return mmmState.STARTING_LINE;
    }
    
    public DraggableLine() {
        setStartX(0.0);
	setStartY(0.0);
	setOpacity(1.0);
	startX = 5.0;
	startY = 5.0;
        startPoint = new DraggableText("OK");
        endPoint = new DraggableText("FANNY");
        
        stations = new LinkedList<>();
        
        startPoint.xProperty().bindBidirectional(this.startXProperty());
        startPoint.yProperty().bindBidirectional(this.startYProperty());
        
        endPoint.xProperty().bindBidirectional(this.endXProperty());
        endPoint.yProperty().bindBidirectional(this.endYProperty());
        
    }
    
    public LinkedList<DraggableCircle> getStations() {
        return stations;
    }
    
    @Override
    public void start(int x, int y) {
        setStartX(x);
	setStartY(y);
    }
    
    @Override
    public void drag(int x, int y) {
        double diffX = x - (startX);
	double diffY = y - startY;
	double newSX = getStartX() + diffX;
	double newSY = getStartY() + diffY;
        double newEX = getEndX() + diffX;
        double newEY = getEndY() + diffY;
	setStartX(newSX);
	setStartY(newSY);
        setEndX(newEX);
        setEndY(newEY);
	startX = x;
	startY = y;
    }
    
    public DraggableText getStartPoint() {
        return startPoint;
    }
    
    public DraggableText getEndPoint() {
        return endPoint;
    }
    
    @Override
    public void size(int x, int y) {
        
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        
    }
    
    @Override
    public String getShapeType() {
        return LINE;
    }

    @Override
    public double getX() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getY() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
