/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import jtps.jTPS_Transaction;
import mmm.gui.mmmWorkspace;

/**
 *
 * @author ralph
 */
public class NewStationTransaction implements jTPS_Transaction{

    private DraggableCircle station;
    mmmData dataManager;
    
    public NewStationTransaction(DraggableCircle s, mmmData data) {
        station = s;
        dataManager = data;
    }
    
    @Override
    public void doTransaction() {
        mmmWorkspace workspace = (mmmWorkspace)dataManager.app.getWorkspaceComponent();
        
        dataManager.getStations().add(station);
        dataManager.getShapes().addAll(station, station.getStationName());
        
        station.getStationName().setStationParent(station);
        
        workspace.getMetroStations().getItems().add(station.name);
        workspace.getMetroStations().getSelectionModel().select(station.name);
        
       
    }

    @Override
    public void undoTransaction() {
        mmmWorkspace workspace = (mmmWorkspace)dataManager.app.getWorkspaceComponent();
        
        workspace.getMetroStations().getSelectionModel().clearSelection();
        workspace.getMetroStations().getItems().remove(station.name);
        
        dataManager.getShapes().removeAll(station.getStationName(), station);
        dataManager.getStations().remove(station);
    }
    
}
