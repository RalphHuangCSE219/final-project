/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;

import djf.components.AppDataComponent;
import djf.AppTemplate;
import java.io.File;
import java.util.LinkedList;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import static javafx.scene.paint.Color.BLACK;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import jtps.jTPS;
import static mmm.data.Draggable.CIRCLE;
import static mmm.data.Draggable.LINE;
import static mmm.data.mmmState.SELECTING_SHAPE;
import mmm.gui.mmmWorkspace;

/**
 *
 * @author ralph
 */
public class mmmData implements AppDataComponent {

    jTPS jtps = new jTPS();
    // THESE ARE THE SHAPES TO DRAW
    ObservableList<Node> shapes;
    LinkedList<DraggableLineWrapper> draggableLineWrappers;
    ObservableList<DraggableLine> draggableLines;
    LinkedList<DraggableCircle> draggableStations;

    // THE BACKGROUND COLOR
    Color backgroundColor;

    // AND NOW THE EDITING DATA
    // THIS IS THE SHAPE CURRENTLY BEING SIZED BUT NOT YET ADDED
    Shape newShape;

    // THIS IS THE SHAPE CURRENTLY SELECTED
    Shape selectedShape;

    //This is the line currently selected
    DraggableLineWrapper selectedLine;

    // FOR FILL AND OUTLINE
    Color currentFillColor;
    Color currentOutlineColor;
    double currentBorderWidth;

    Color stationColor;
    Color lineColor;

    // CURRENT STATE OF THE APP
    mmmState state;

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    // USE THIS WHEN THE SHAPE IS SELECTED
    Effect highlightedEffect;

    public static final String WHITE_HEX = "#FFFFFF";
    public static final String BLACK_HEX = "#000000";
    public static final String YELLOW_HEX = "#EEEE00";
    public static final Paint DEFAULT_BACKGROUND_COLOR = Paint.valueOf(WHITE_HEX);
    public static final Paint HIGHLIGHTED_COLOR = Paint.valueOf(YELLOW_HEX);
    public static final int HIGHLIGHTED_STROKE_THICKNESS = 3;

    @Override
    public void resetData() {
        setState(SELECTING_SHAPE);
        newShape = null;
        selectedShape = null;
        selectedLine = null;

        // INIT THE COLORS
        currentFillColor = Color.web(BLACK_HEX);
        currentOutlineColor = Color.web(BLACK_HEX);

        shapes.clear();
        ((mmmWorkspace) app.getWorkspaceComponent()).getCanvas().getChildren().clear();
        ((mmmWorkspace) app.getWorkspaceComponent()).resetWorkspace();
    }

    public mmmData(AppTemplate initApp) {
        app = initApp;

        // NO SHAPE STARTS OUT AS SELECTED
        newShape = null;
        selectedShape = null;
        selectedLine = null;

        // INIT LISTS
        draggableLineWrappers = new LinkedList<>();
        draggableStations = new LinkedList<>();

        // INIT THE COLORS
        currentFillColor = Color.web(WHITE_HEX);
        currentOutlineColor = Color.web(BLACK_HEX);
        currentBorderWidth = 1;

        // THIS IS FOR THE SELECTED SHAPE
        DropShadow dropShadowEffect = new DropShadow();
        dropShadowEffect.setOffsetX(0.0f);
        dropShadowEffect.setOffsetY(0.0f);
        dropShadowEffect.setSpread(1.0);
        dropShadowEffect.setColor(Color.YELLOW);
        dropShadowEffect.setBlurType(BlurType.GAUSSIAN);
        dropShadowEffect.setRadius(5);
        highlightedEffect = dropShadowEffect;
    }

    public ObservableList<Node> getShapes() {
        return shapes;
    }

    public DraggableLineWrapper getSelectedLine() {
        return selectedLine;
    }

    public void setSelectedLine(DraggableLineWrapper line) {
        selectedLine = line;
    }

    public LinkedList<DraggableLineWrapper> getDraggableLines() {
        return draggableLineWrappers;
    }

    public ObservableList<DraggableLine> getLines() {
        return draggableLines;
    }

    public LinkedList<DraggableCircle> getStations() {
        return draggableStations;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public Color getCurrentFillColor() {
        return currentFillColor;
    }

    public Color getCurrentOutlineColor() {
        return currentOutlineColor;
    }

    public double getCurrentBorderWidth() {
        return currentBorderWidth;
    }

    public void setShapes(ObservableList<Node> initShapes) {
        shapes = initShapes;
    }

    public void setBackgroundColor(Color initBackgroundColor) {

        backgroundColor = initBackgroundColor;

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        BackgroundFill fill = new BackgroundFill(backgroundColor, null, null);
        Background background = new Background(fill);
        canvas.setBackground(background);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void setCurrentFillColor(Color initColor) {
        currentFillColor = initColor;
        if (selectedShape instanceof DraggableText) {
            selectedShape.setFill(currentFillColor);
        }
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void setCurrentOutlineColor(Color initColor) {
        currentOutlineColor = initColor;
        if (selectedShape != null) {
            selectedShape.setStroke(initColor);
        }
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void setCurrentOutlineThickness(int initBorderWidth) {
        currentBorderWidth = initBorderWidth;
        if (selectedShape != null) {
            selectedShape.setStrokeWidth(initBorderWidth);
        }
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void removeSelectedShape() {
        if (selectedShape != null) {
            shapes.remove(selectedShape);
            selectedShape = null;
        }
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void moveSelectedShapeToBack() {
        if (selectedShape != null) {
            shapes.remove(selectedShape);
            if (shapes.isEmpty()) {
                shapes.add(selectedShape);
            } else {
                ArrayList<Node> temp = new ArrayList<>();
                temp.add(selectedShape);
                for (Node node : shapes) {
                    temp.add(node);
                }
                shapes.clear();
                for (Node node : temp) {
                    shapes.add(node);
                }
            }
        }
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void moveSelectedShapeToFront() {
        if (selectedShape != null) {
            shapes.remove(selectedShape);
            shapes.add(selectedShape);
        }
    }

    public Shape getNewShape() {
        return newShape;
    }

    public Shape getSelectedShape() {
        return selectedShape;
    }

    public void setSelectedShape(Shape initSelectedShape) {
        try {
            selectedShape = initSelectedShape;
        } catch (Exception e) {

        }
    }

    public void unhighlightShape(Shape shape) {
        shape.setEffect(null);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void highlightShape(Shape shape) {
        shape.setEffect(highlightedEffect);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public Shape selectTopShape(int x, int y) {
        Shape shape = getTopShape(x, y);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
        if (shape == selectedShape) {
            return shape;
        }

        if (selectedShape != null) {
            unhighlightShape(selectedShape);
        }
        if (shape != null) {
            highlightShape(shape);
            mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
            //if(selectedShape)
//            workspace.loadSelectedShapeSettings(shape);

            // workspace.loadSelectedShapeSettings(shape);
        }
        selectedShape = shape;
        if (shape != null) {
            if (!(shape instanceof Line)) {
                ((Draggable) shape).start(x, y);
            }
        }
        return shape;
    }

    public Shape getTopShape(int x, int y) {
        for (int i = shapes.size() - 1; i >= 0; i--) {
            Shape shape = (Shape) shapes.get(i);

            if (shape.contains(x, y)) {
                if (shape instanceof Line) {
                    for (DraggableLineWrapper draggableLineWrapper : draggableLineWrappers) {
                        if (draggableLineWrapper.contains(shape)) {
                            selectedLine = draggableLineWrapper;
                            for (int j = 0; j < shapes.size(); j++) {
                                unhighlightShape((Shape) shapes.get(j));
                            }
                            for (Object objectsInsideLine : draggableLineWrapper) {
                                highlightShape(((Shape) objectsInsideLine));
                            }
                        }
                    }
                    return shape;
                } else {
                    if (shape instanceof DraggableCircle) {
                        if (((DraggableCircle) shape).getLineParent() != null) {
                            selectedLine = ((DraggableCircle) shape).getLineParent();
                        }
                    } else {
                        selectedLine = null;
                    }
                    ((Draggable) shape).start(x, y);
                    return shape;
                }
            }
        }
        for (Object tests : shapes) {
            if (tests instanceof Shape) {
                unhighlightShape((Shape) tests);
            }
        }
        return null;
    }

    public void addShape(Shape shapeToAdd) {
        shapes.add(shapeToAdd);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void removeShape(Shape shapeToRemove) {
        if (shapeToRemove instanceof DraggableCircle) {
            System.out.println("Test");
        }
        shapes.remove(shapeToRemove);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public mmmState getState() {
        return state;
    }

    public void setState(mmmState initState) {
        state = initState;
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public boolean isInState(mmmState testState) {
        return state == testState;
    }

    public jTPS getJTPS() {
        return jtps;
    }

    public void setStationColor(Color selectedColor) {
        stationColor = selectedColor;
        if (selectedShape != null) {
            selectedShape.setFill(selectedColor);
        }
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void setLineColor(Color selectedColor) {
        lineColor = selectedColor;
        if (selectedLine != null) {
            for (int i = 0; i < selectedLine.size(); i++) {
                if (selectedLine.get(i) instanceof Line) {
                    ((Line) selectedLine.get(i)).setStroke(selectedColor);
                }
            }
        }
        selectedLine.setColor(lineColor);
        app.getGUI().getFileController().markAsEdited(app.getGUI());
    }

    public void startNewLine(int x, int y) {

        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        Dialog lineDialog = new Dialog<>();
        lineDialog.setTitle("Create Line");
        lineDialog.setHeaderText(null);
        lineDialog.setResizable(false);

        Label lineNameLabel = new Label("Enter the name of this line");
        TextField textField = new TextField();

        ColorPicker newLineColorPicker = new ColorPicker(workspace.getLineColorPicker().getValue());
        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(8);
        grid.add(lineNameLabel, 1, 1);
        grid.add(textField, 2, 1);
        grid.add(newLineColorPicker, 2, 2);
        grid.setPadding(new Insets(50, 50, 10, 10));

        lineDialog.getDialogPane().setContent(grid);
        lineDialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        (ButtonType.OK).getButtonData().isDefaultButton();
        lineDialog.showAndWait();

        DraggableLineWrapper newLine = new DraggableLineWrapper(textField.getText(), newLineColorPicker.getValue(), workspace.getLineThicknessSlider().getValue());

        draggableLineWrappers.add(newLine);
        ((DraggableText) newLine.get(0)).setX(x);
        ((DraggableText) newLine.get(0)).setY(y);

        CreateLineTransaction clt = new CreateLineTransaction(newLine, this);
        this.getJTPS().addTransaction(clt);
        state = SELECTING_SHAPE;

    }

    public void startNewStation(int x, int y) {
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();

        DraggableCircle newCircle = new DraggableCircle();
        newCircle.start(x, y);
        newCircle.setCenterX(x);
        newCircle.setCenterY(y);

        newShape = newCircle;

        initNewShape();
        NewStationTransaction nst = new NewStationTransaction((DraggableCircle) newShape, this);
        this.getJTPS().addTransaction(nst);
        state = SELECTING_SHAPE;
    }

    public void startNewText(int x, int y) {
        DraggableText newText = new DraggableText();
        newText.start(x, y);
        newText.setX(x);
        newText.setY(y);
        newShape = newText;
        ((DraggableText) newShape).setFont(Font.font("Arial", FontWeight.NORMAL, FontPosture.ITALIC, 10));
        initNewShape();
        NewTextTransaction ntt = new NewTextTransaction((DraggableText)newShape, this);
        this.getJTPS().addTransaction(ntt);
        state = SELECTING_SHAPE;
        mmmWorkspace workspace = (mmmWorkspace)app.getWorkspaceComponent();
        workspace.getFontFamilies().getSelectionModel().select(((DraggableText)newShape).getFont().getFamily());
        workspace.getFontSizes().getSelectionModel().select(((Integer)(int)((DraggableText)newShape).getFont().getSize()));
    }

    public void startNewImage(int x, int y) {
        DraggableImage newImage = new DraggableImage();
        newImage.start(x, y);
        newImage.setX(x);
        newImage.setY(y);
        newShape = newImage;
        newShape.setStroke(Color.BLACK);
        newShape.setStrokeWidth(0.75);
        
        initNewShape();
        NewImageTransaction nit = new NewImageTransaction((DraggableImage) newShape, this);
        this.getJTPS().addTransaction(nit);
        state = SELECTING_SHAPE;
    }

    public void initNewShape() {
        if (selectedShape != null) {
            unhighlightShape(selectedShape);
            selectedShape = null;
        }
        mmmWorkspace workspace = (mmmWorkspace) app.getWorkspaceComponent();
        if (newShape instanceof DraggableCircle) {
            Dialog newStationDialog = new Dialog<>();
            newStationDialog.setTitle("Create a Station");
            TextField stationName = new TextField("Enter the name of the station");
            newStationDialog.getDialogPane().setContent(stationName);
            newStationDialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
            (ButtonType.OK).getButtonData().isDefaultButton();
            newStationDialog.showAndWait();
            newShape.setFill(workspace.getStationColorPicker().getValue());
            newShape.setStroke(BLACK);
            ((DraggableCircle) newShape).setRadius(workspace.getRadiusSlider().getValue());
            ((DraggableCircle) newShape).setName(stationName.getText());
            ((DraggableCircle) newShape).getStationName().setFill(workspace.getFillColorPicker().getValue());
            DraggableText test = ((DraggableCircle) newShape).getStationName();
            FontWeight testFontWeight;
            FontPosture testFontPosture;
            if (test.getFont().getName().contains("Bold")) {
                testFontWeight = FontWeight.BOLD;
            } else {
                testFontWeight = FontWeight.NORMAL;
            }

            if (test.getFont().getName().contains("Italic")) {
                testFontPosture = FontPosture.ITALIC;
            } else {
                testFontPosture = FontPosture.REGULAR;
            }

            ((DraggableCircle) newShape).getStationName().setFont(Font.font(test.getFont().getFamily(), testFontWeight, testFontPosture, test.getFont().getSize()));

        } else if (newShape instanceof DraggableText) {
            Dialog newTextDialog = new Dialog<>();
            newTextDialog.setTitle("Create a Label");
            Label ftextLabel = new Label("Enter your text");
            TextField textField = new TextField("Enter the name of the label");
            Label fsizeLabel = new Label("Font Size");
            ComboBox<Integer> fontSizeBox = new ComboBox<>();
            fontSizeBox.getItems().addAll(10, 11, 12, 14, 16, 18, 20, 22, 24, 28, 32, 36, 40, 46, 52, 58, 64, 72, 80);
            fontSizeBox.setPromptText("Choose a font size");
            Label ftypeLabel = new Label("Font Type");
            ComboBox<String> fontTypeBox = new ComboBox<>();
            fontTypeBox.getItems().addAll("Arial",
                    "Calibri",
                    "Century",
                    "Cambria",
                    "Courier",
                    "Helvetica",
                    "Times New Roman",
                    "Verdana"
            );
            fontTypeBox.setPromptText("Choose a font type");

            CheckBox boldCheckBox = new CheckBox("Bold");
            CheckBox italicCheckBox = new CheckBox("Italic");

            GridPane grid = new GridPane();
            grid.setVgap(4);
            grid.setHgap(8);
            grid.add(ftextLabel, 1, 1);
            grid.add(textField, 2, 1);
            grid.add(ftypeLabel, 1, 2);
            grid.add(fontTypeBox, 2, 2);
            grid.add(fsizeLabel, 1, 3);
            grid.add(fontSizeBox, 2, 3);
            grid.add(boldCheckBox, 3, 1);
            grid.add(italicCheckBox, 3, 2);
            grid.setPadding(new Insets(20, 50, 10, 10));

            newTextDialog.getDialogPane().setContent(grid);
            newTextDialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
            newTextDialog.showAndWait();

            ((DraggableText) newShape).setText(textField.getText());

            if (boldCheckBox.isSelected() && italicCheckBox.isSelected()) {
                ((DraggableText) newShape).setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.BOLD, FontPosture.ITALIC, fontSizeBox.getSelectionModel().getSelectedItem()));
            } else if (boldCheckBox.isSelected()) {
                ((DraggableText) newShape).setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.BOLD, FontPosture.REGULAR, fontSizeBox.getSelectionModel().getSelectedItem()));
            } else if (italicCheckBox.isSelected()) {
                ((DraggableText) newShape).setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.NORMAL, FontPosture.ITALIC, fontSizeBox.getSelectionModel().getSelectedItem()));
            } else {
                ((DraggableText) newShape).setFont(Font.font(fontTypeBox.getSelectionModel().getSelectedItem(), FontWeight.NORMAL, FontPosture.REGULAR, fontSizeBox.getSelectionModel().getSelectedItem()));
            }

            //shapes.add(newShape);

        } else if (newShape instanceof DraggableImage) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose Image File");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                    new FileChooser.ExtensionFilter("All Files", "*.*"));
            File selectedFile = fileChooser.showOpenDialog(app.getGUI().getWindow());
            if (selectedFile != null) {
                Image selectedImage = new Image("file:///" + selectedFile.getAbsoluteFile());
                ((DraggableImage) newShape).setFilePath("file:///" + selectedFile.getAbsoluteFile());
                ((DraggableImage) newShape).setWidth(selectedImage.getWidth());
                ((DraggableImage) newShape).setHeight(selectedImage.getHeight());
                ((DraggableImage) newShape).setFill(new ImagePattern(selectedImage));
            }
            //shapes.add(newShape);
        }
        state = mmmState.SELECTING_SHAPE;
    }
}
