/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author ralph
 */
public class DraggableText extends Text implements Draggable {
    
    DraggableLineWrapper lineParent;
    DraggableCircle stationParent;
    double startX;
    double startY;
    
    public DraggableText() {
	setX(0.0);
	setY(0.0);
        setWrappingWidth(0.0);
	setOpacity(1.0);
	startX = 0.0;
	startY = 0.0;
        setFont(Font.font("Arial", FontWeight.NORMAL, FontPosture.ITALIC, 10));
    }
    
    public DraggableText(String name){
        setX(0.0);
	setY(0.0);
        setWrappingWidth(0.0);
	setOpacity(1.0);
	startX = 0.0;
	startY = 0.0;
        
        setText(name);
        setFont(Font.font("Arial", FontWeight.NORMAL, FontPosture.ITALIC, 10));
    }
    
    @Override
    public mmmState getStartingState() {
	return mmmState.STARTING_TEXT;
    }
    
    @Override
    public void start(int x, int y) {
	startX = x;
	startY = y;
    }
    
    
    @Override
    public double getWidth() {
        return this.getWrappingWidth();
    }

    @Override
    public double getHeight() {
        return this.getFont().getSize(); 
    }

    @Override
    public void drag(int x, int y) {
	double diffX = x - startX;
	double diffY = y - startY;
	double newX = getX() + diffX;
	double newY = getY() + diffY;
	setX(newX);
	setY(newY);
	startX = x;
	startY = y;
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - getX();
	double height = y - getY();
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	xProperty().set(initX);
	yProperty().set(initY);
    }
    
    @Override
    public String getShapeType() {
	return TEXT;
    }
    
    public void setLineParent(DraggableLineWrapper line) {
        lineParent = line;
    }
    
    public DraggableLineWrapper getLineParent() {
        return lineParent;
    }
    
    public void setStationParent(DraggableCircle station) {
        stationParent = station;
    }
    
    public DraggableCircle getStationParent() {
        return stationParent;
    }

}
