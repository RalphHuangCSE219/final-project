/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *
 * @author ralph
 */
public class LineColorTransaction implements jTPS_Transaction {

    private Color initColor;
    private Color finalColor;
    mmmData dataManager;
    DraggableLineWrapper s;
    
    public LineColorTransaction(Color initFillColor, Color finalFillColor, mmmData data, DraggableLineWrapper shape)
    {
        initColor = initFillColor;
        finalColor = finalFillColor;
        dataManager = data;
        s = shape;
    }
    
    @Override
    public void doTransaction() {
        for(int i = 0; i < s.size(); i++) {
            if(s.get(i) instanceof Line)
                ((Line)s.get(i)).setStroke(finalColor);
        }
        
    }

    @Override
    public void undoTransaction() {
        for(int i = 0; i < s.size(); i++) {
            if(s.get(i) instanceof Line)
                ((Line)s.get(i)).setStroke(initColor);
        }
    }
    
}
