/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import jtps.jTPS_Transaction;
import mmm.gui.mmmWorkspace;

/**
 *
 * @author ralph
 */
public class NewImageTransaction implements jTPS_Transaction{
    
    private DraggableImage image;
    mmmData dataManager;
    
    public NewImageTransaction(DraggableImage i, mmmData data) {
        image = i;
        dataManager = data;
    }
    
    public void doTransaction() {
        mmmWorkspace workspace = (mmmWorkspace)dataManager.app.getWorkspaceComponent(); 
        dataManager.getShapes().add(image);
    }
    
    public void undoTransaction() {
        mmmWorkspace workspace = (mmmWorkspace)dataManager.app.getWorkspaceComponent();
        dataManager.getShapes().remove(image);
    }
}
