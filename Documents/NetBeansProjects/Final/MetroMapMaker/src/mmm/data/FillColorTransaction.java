/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;

/**
 *
 * @author ralph
 */
public class FillColorTransaction implements jTPS_Transaction{

    private Color initColor;
    private Color finalColor;
    mmmData dataManager;
    Shape s;
    
    public FillColorTransaction(Color initFillColor, Color finalFillColor, mmmData data, Shape shape)
    {
        initColor = initFillColor;
        finalColor = finalFillColor;
        dataManager = data;
        s = shape;
    }
    
    @Override
    public void doTransaction() {
        ((Shape) dataManager.getShapes().get(dataManager.getShapes().indexOf(s))).setFill(finalColor);
        
    }

    @Override
    public void undoTransaction() {
        ((Shape) dataManager.getShapes().get(dataManager.getShapes().indexOf(s))).setFill(initColor);
    }
    
}
