/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 *
 * @author ralph
 */
public enum mmmState {
    SELECTING_SHAPE,
    DRAGGING_SHAPE,
    STARTING_LINE,
    STARTING_STATION,
    DRAGGING_NOTHING,
    DRAGGING_TEXT,
    STARTING_TEXT,
    STARTING_IMAGE, 
    STARTING_CIRCLE, 
    STARTING_RECTANGLE,
    ADDING_STATION_TO_LINE, 
    REMOVING_STATION_FROM_LINE,
}
