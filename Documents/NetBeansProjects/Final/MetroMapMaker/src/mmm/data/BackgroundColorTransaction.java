/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 *
 * @author ralph
 */
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

public class BackgroundColorTransaction implements jTPS_Transaction{

    private Color initColor;
    private Color finalColor;
    mmmData dataManager;
    
    public BackgroundColorTransaction(Color initBGColor, Color finalBGColor, mmmData data)
    {
        initColor = initBGColor;
        finalColor = finalBGColor;
        dataManager = data;
    }
    
    @Override
    public void doTransaction() {
        dataManager.setBackgroundColor(finalColor);
        
    }

    @Override
    public void undoTransaction() {
        dataManager.setBackgroundColor(initColor);
    }
    
}
