/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

/**
 *
 * @author ralph
 */
import javafx.scene.shape.Shape;
import jtps.jTPS_Transaction;
import mmm.gui.mmmWorkspace;


public class CreateLineTransaction implements jTPS_Transaction {
    
    private DraggableLineWrapper line;
    mmmData dataManager;

    public CreateLineTransaction(DraggableLineWrapper lineCreated, mmmData data) {
        line = lineCreated;
        dataManager = data;
    }
    @Override
    public void doTransaction() {
        dataManager.getDraggableLines().add(line);
        
        for (int i = 0; i < line.size(); i++) {
            if (line.get(i) != null) {
                dataManager.newShape = (Shape) line.get(i);
                dataManager.getShapes().add(dataManager.getNewShape());
            }
        }
        mmmWorkspace workspace = (mmmWorkspace)dataManager.app.getWorkspaceComponent();
        workspace.getMetroLines().getItems().add(line.name);
        workspace.getMetroLines().getSelectionModel().select(line.name);
    }

    @Override
    public void undoTransaction() {
        mmmWorkspace workspace = (mmmWorkspace)dataManager.app.getWorkspaceComponent();
        workspace.getMetroLines().getSelectionModel().clearSelection();
        workspace.getMetroLines().getItems().remove(line.name);
        
        dataManager.getShapes().remove(dataManager.getShapes().indexOf(line.getFirst()), dataManager.getShapes().indexOf(line.getLast())+1);
        dataManager.getDraggableLines().remove(line);
    }
    
    
}
