/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import java.util.LinkedList;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;

/**
 *
 * @author ralph
 */
public class DraggableLineWrapper extends LinkedList {

    boolean circular;
    public double width;
    public Color color;
    public String name;
    public LinkedList<DraggableCircle> stations;
    public DraggableText startPoint;
    public DraggableText endPoint;

    public DraggableLineWrapper(String s, Color x, double r) {
        this.circular = false;
        this.name = s;
        this.color = x;
        this.width = r;
        stations = new LinkedList<>();
        Line defaultLine = new Line(10, 10, 400, 400);
        defaultLine.setStroke(color);
        defaultLine.setStrokeWidth(r);
        startPoint = new DraggableText(name);
        endPoint = new DraggableText(name);

        startPoint.setLineParent(this);
        endPoint.setLineParent(this);

        startPoint.xProperty().bindBidirectional(defaultLine.startXProperty());
        startPoint.yProperty().bindBidirectional(defaultLine.startYProperty());
        endPoint.xProperty().bindBidirectional(defaultLine.endXProperty());
        endPoint.yProperty().bindBidirectional(defaultLine.endYProperty());

        this.add(startPoint);
        this.add(defaultLine);
        this.add(endPoint);

    }

    public DraggableLineWrapper(String n, LinkedList<DraggableCircle> s) {
        startPoint = new DraggableText(n);
        endPoint = new DraggableText(n);

        startPoint.setLineParent(this);
        endPoint.setLineParent(this);

        this.name = n;
        this.stations = s;

        for (int i = 0; i < s.size(); i++) {
            if (i == 0) {
                Line firstLine = new Line(10, 10, s.getFirst().startCenterX, s.getFirst().startCenterY);
                startPoint.xProperty().bindBidirectional(firstLine.startXProperty());
                startPoint.yProperty().bindBidirectional(firstLine.startYProperty());
                this.add(startPoint);
                this.add(firstLine);
                this.add(s.getFirst());
                s.getFirst().centerXProperty().bindBidirectional(firstLine.endXProperty());
                s.getFirst().centerYProperty().bindBidirectional(firstLine.endYProperty());

                if (s.size() == 1) {
                    Line lastLine = new Line(s.getLast().startCenterX, s.getLast().startCenterY, 400, 400);
                    this.add(lastLine);
                    lastLine.startXProperty().bindBidirectional(s.getLast().centerXProperty());
                    lastLine.startYProperty().bindBidirectional(s.getLast().centerYProperty());

                    endPoint.xProperty().bindBidirectional(lastLine.endXProperty());
                    endPoint.yProperty().bindBidirectional(lastLine.endYProperty());
                    this.add(endPoint);
                }
            } else if (s.size() > 1 && i == (s.size() - 1)) {

                Line connector = new Line(s.get(s.size() - 2).startCenterX, s.get(s.size() - 2).startCenterY, s.getLast().startCenterX, s.getLast().startCenterY);
                this.add(connector);

                s.get(i - 1).centerXProperty().bindBidirectional(connector.startXProperty());
                s.get(i - 1).centerYProperty().bindBidirectional(connector.startYProperty());

                this.add(s.getLast());
                s.getLast().centerXProperty().bindBidirectional(connector.endXProperty());
                s.getLast().centerYProperty().bindBidirectional(connector.endYProperty());
                Line lastLine = new Line(s.getLast().startCenterX, s.getLast().startCenterY, 400, 400);
                this.add(lastLine);
                lastLine.startXProperty().bindBidirectional(s.getLast().centerXProperty());
                lastLine.startYProperty().bindBidirectional(s.getLast().centerYProperty());

                endPoint.xProperty().bindBidirectional(lastLine.endXProperty());
                endPoint.yProperty().bindBidirectional(lastLine.endYProperty());
                this.add(endPoint);
            } else {
                Line inBetweenLine = new Line(s.get(i - 1).startCenterX, s.get(i - 1).startCenterY, s.get(i).startCenterX, s.get(i).startCenterY);
                this.add(inBetweenLine);
                inBetweenLine.startXProperty().bindBidirectional(s.get(i - 1).centerXProperty());
                inBetweenLine.startYProperty().bindBidirectional(s.get(i - 1).centerYProperty());
                this.add(s.get(i));
                s.get(i).centerXProperty().bindBidirectional(inBetweenLine.endXProperty());
                s.get(i).centerYProperty().bindBidirectional(inBetweenLine.endYProperty());
            }
        }
    }

    public String getName() {
        return this.name;
    }

    public void setName(String n) {
        this.startPoint.setText(n);
        this.endPoint.setText(n);
        this.name = n;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color colorToSet) {
        this.color = colorToSet;
        for(int i = 0; i < this.size(); i++) {
            if(this.get(i) instanceof Line)
                ((Line)(this.get(i))).setStroke(colorToSet);
        }
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double w) {
        this.width = w;
    }

    public LinkedList<DraggableCircle> getStations() {
        return stations;
    }

    public void setStations(LinkedList<DraggableCircle> s) {
        stations = s;
    }

    public DraggableText getStartPoint() {
        return startPoint;
    }

    public DraggableText getEndPoint() {
        return endPoint;
    }
    
    public boolean getCircular() {
        return this.circular;
    }
    
    public void setCircular(boolean t) {
        this.circular = t;
    }

    public DraggableLineWrapper addStationToLine(DraggableCircle newStation) {
        double distance[] = new double[stations.size()];
        int pointer1 = 0;
        int pointer2 = 0;

        if (stations.size() > 0) {
            for (int i = 0; i < stations.size(); i++) {
                double d = Math.sqrt(Math.pow(newStation.startCenterX - (stations.get(i).startCenterX), 2.0) + Math.pow(newStation.startCenterY - (stations.get(i).startCenterY), 2.0));
                distance[i] = d;
            }
            double dtracker = distance[0];
            for (int i = 0; i < stations.size(); i++) {
                if (distance[i] < dtracker) {
                    pointer2 = pointer1;
                    dtracker = distance[i];
                    pointer1 = i;
                }
            }
            stations.add(pointer1, newStation);
        } else {
            stations.add(newStation);
        }

        DraggableLineWrapper newLine = new DraggableLineWrapper(name, stations);
        newStation.setLineParent(newLine);
        return newLine;
    }

    public DraggableLineWrapper removeStationFromLine(DraggableCircle stationToRemove) {
        if (stationToRemove != null) {
            stations.remove(stationToRemove);
            int x = this.indexOf(stationToRemove);
            Line connector = new Line();
            connector.setStartX(((Line) (this.get(x - 1))).getStartX());
            connector.setStartY(((Line) (this.get(x - 1))).getStartY());
            connector.setEndX(((Line) (this.get(x + 1))).getEndX());
            connector.setEndY(((Line) (this.get(x + 1))).getEndY());

            if (this.get(x - 2) instanceof DraggableText && this.get(x + 2) instanceof DraggableText) {
                this.remove(x + 1);
                this.add(x + 1, connector);
                this.remove(x);
                this.remove(x - 1);
                int y = this.indexOf(connector);
                ((DraggableText) (this.get(y - 1))).xProperty().bindBidirectional(connector.startXProperty());
                ((DraggableText) (this.get(y - 1))).yProperty().bindBidirectional(connector.startYProperty());

                ((DraggableText) (this.get(y + 1))).xProperty().bindBidirectional(connector.endXProperty());
                ((DraggableText) (this.get(y + 1))).yProperty().bindBidirectional(connector.endYProperty());

            } else if (this.get(x - 2) instanceof DraggableCircle && this.get(x + 2) instanceof DraggableText) {
                this.remove(x + 1);
                this.add(x + 1, connector);
                this.remove(x);
                this.remove(x - 1);
                int y = this.indexOf(connector);
                ((DraggableCircle) (this.get(y - 1))).centerXProperty().bindBidirectional(connector.startXProperty());
                ((DraggableCircle) (this.get(y - 1))).centerYProperty().bindBidirectional(connector.startYProperty());

                ((DraggableText) (this.get(y + 1))).xProperty().bindBidirectional(connector.endXProperty());
                ((DraggableText) (this.get(y + 1))).yProperty().bindBidirectional(connector.endYProperty());

            } else if (this.get(x - 2) instanceof DraggableText && this.get(x + 2) instanceof DraggableCircle) {
                this.remove(x + 1);
                this.add(x + 1, connector);
                this.remove(x);
                this.remove(x - 1);
                int y = this.indexOf(connector);
                ((DraggableText) (this.get(y - 1))).xProperty().bindBidirectional(connector.startXProperty());
                ((DraggableText) (this.get(y - 1))).yProperty().bindBidirectional(connector.startYProperty());

                ((DraggableCircle) (this.get(y + 1))).centerXProperty().bindBidirectional(connector.endXProperty());
                ((DraggableCircle) (this.get(y + 1))).centerYProperty().bindBidirectional(connector.endYProperty());
            } else if (this.get(x - 2) instanceof DraggableCircle && this.get(x + 2) instanceof DraggableCircle) {
                this.remove(x + 1);
                this.add(x + 1, connector);
                this.remove(x);
                this.remove(x - 1);
                int y = this.indexOf(connector);
                ((DraggableCircle) (this.get(y - 1))).centerXProperty().bindBidirectional(connector.startXProperty());
                ((DraggableCircle) (this.get(y - 1))).centerYProperty().bindBidirectional(connector.startYProperty());

                ((DraggableCircle) (this.get(y + 1))).centerXProperty().bindBidirectional(connector.endXProperty());
                ((DraggableCircle) (this.get(y + 1))).centerYProperty().bindBidirectional(connector.endYProperty());
            }
            return this;
        } else {
            return null;
        }
    }
}
