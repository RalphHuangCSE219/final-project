package mmm.data;

/**
 *
 * @author ralph
 */
public interface Draggable {
    public static final String RECTANGLE = "RECTANGLE";
    public static final String CIRCLE = "CIRCLE";
    public static final String TEXT = "TEXT";
    public static final String IMAGE = "IMAGE";
    public static final String LINE = "LINE";
    public mmmState getStartingState();
    public void start(int x, int y);
    public void drag(int x, int y);
    public void size(int x, int y);
    public double getX();
    public double getY();
    public double getWidth();
    public double getHeight();
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight);
    public String getShapeType();
}
