/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import jtps.jTPS_Transaction;

/**
 *
 * @author ralph
 */
public class NewTextTransaction implements jTPS_Transaction{
    
    private DraggableText text;
    mmmData dataManager;
    
    public NewTextTransaction(DraggableText t, mmmData data) {
        text = t;
        dataManager = data;
    }
    
    public void doTransaction() {
        dataManager.getShapes().add(text);
    }
    
    public void undoTransaction() {
        dataManager.getShapes().remove(text);
    }
}
