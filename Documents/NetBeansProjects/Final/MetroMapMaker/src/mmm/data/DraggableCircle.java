/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.data;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.shape.Circle;

/**
 *
 * @author ralph
 */
public class DraggableCircle extends Circle implements Draggable{
    double startCenterX;
    double startCenterY;
    DraggableText stationName;
    
    DraggableLineWrapper lineParent;
    
    String name;
    
    public String getName() {
        return name;
    }
    
    public void setName(String s) {
        name = s;
        stationName.setText(s);
    }
    
    public DraggableCircle() {
	setCenterX(0.0);
	setCenterY(0.0);
	setRadius(0.0);
	setOpacity(1.0);
	startCenterX = 0.0;
	startCenterY = 0.0;
        stationName = new DraggableText("OK");
        
        stationName.xProperty().bindBidirectional(new SimpleDoubleProperty(this.getCenterX() + 4 *this.getRadius()));
        stationName.yProperty().bindBidirectional(new SimpleDoubleProperty(this.getCenterY() - 4 *this.getRadius()));
        
        this.centerXProperty().bindBidirectional(stationName.xProperty());
        this.centerYProperty().bindBidirectional(stationName.yProperty());
    }
    
    @Override
    public mmmState getStartingState() {
	return mmmState.STARTING_STATION;
    }
    
    @Override
    public void start(int x, int y) {
	startCenterX = x;
	startCenterY = y;
        
    }
    
    @Override
    public void drag(int x, int y) {
	double diffX = x - startCenterX;
	double diffY = y - startCenterY;
	double newX = getCenterX() + diffX;
	double newY = getCenterY() + diffY;
	setCenterX(newX);
	setCenterY(newY);
	startCenterX = x;
	startCenterY = y;
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - startCenterX;
	double height = y - startCenterY;
	double centerX = startCenterX + (width / 2);
	double centerY = startCenterY + (height / 2);
	setCenterX(centerX);
	setCenterY(centerY);
	setRadius(width / 2);
	setRadius(height / 2);	
	
    }
        
    @Override
    public double getX() {
	return getCenterX() - getRadius();
    }

    @Override
    public double getY() {
	return getCenterY() - getRadius();
    }

    @Override
    public double getWidth() {
	return getRadius() * 2;
    }

    @Override
    public double getHeight() {
	return getRadius() * 2;
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	setCenterX(initX + (initWidth/2));
	setCenterY(initY + (initHeight/2));
	setRadius(initWidth/2);
	setRadius(initHeight/2);
    }
    
    public DraggableText getStationName() {
        return stationName;
    }
    
    public DraggableLineWrapper getLineParent() {
        return lineParent;
    }
    
    public void setLineParent(DraggableLineWrapper line) {
        lineParent = line;
    }
    
    @Override
    public String getShapeType() {
	return CIRCLE;
    }
}
