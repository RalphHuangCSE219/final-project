/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mmm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.util.LinkedList;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import mmm.data.Draggable;
import static mmm.data.Draggable.CIRCLE;
import static mmm.data.Draggable.LINE;
import static mmm.data.Draggable.TEXT;
import mmm.data.DraggableCircle;
import mmm.data.DraggableImage;
import mmm.data.DraggableLine;
import mmm.data.DraggableLineWrapper;
import mmm.data.DraggableText;
import mmm.data.mmmData;
/**
 *
 * @author ralph
 */
public class mmmFiles implements AppFileComponent {
    
    // FOR JSON LOADING
    static final String JSON_BG_COLOR = "background_color";
    static final String JSON_COLOR = "color";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_ALPHA = "alpha";
    static final String JSON_SHAPES = "shapes";
    static final String JSON_SHAPE = "shape";
    static final String JSON_TYPE = "type";
    static final String JSON_X = "x";
    static final String JSON_Y = "y";
    static final String JSON_STARTX = "startx";
    static final String JSON_STARTY = "starty";
    static final String JSON_ENDX = "endx";
    static final String JSON_ENDY = "endy";
    static final String JSON_WIDTH = "width";
    static final String JSON_HEIGHT = "height";
    static final String JSON_FILL_COLOR = "fill_color";
    static final String JSON_OUTLINE_COLOR = "outline_color";
    static final String JSON_OUTLINE_THICKNESS = "outline_thickness";
    static final String JSON_NAME = "name";
    static final String JSON_LINES = "lines";
    static final String JSON_STATIONS = "stations";
    static final String JSON_CIRCULAR = "circular";
    
    static final String JSON_STATION_NAMES = "station_names";

    static final String DEFAULT_DOCTYPE_DECLARATION = "<!doctype html>\n";
    static final String DEFAULT_ATTRIBUTE_VALUE = "";
    private final String JSON_FSIZE = "font_size";
    private final String JSON_FNAME = "font_name";
    private final String JSON_FONT = "font";
    private final String JSON_TEXT = "text";
    private final String JSON_IMG_PATH = "fill_img";
    
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        
        mmmData dataManager = (mmmData)data;
        
        // FIRST THE BACKGROUND COLOR
        Color bgColor;
        if (dataManager.getBackgroundColor() != null)
            bgColor = dataManager.getBackgroundColor();
        else
            bgColor = Color.WHITE;
        JsonObject bgColorJson = makeJsonColorObject(bgColor);
        
        //THEN THE SHAPES
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        ObservableList<Node> shapes = dataManager.getShapes();
        for(Node node: shapes) {
            Shape shape = (Shape) node;
            if (shape instanceof DraggableText) {
                double x = ((DraggableText) shape).getX();
                double y= ((DraggableText) shape).getY();
                double width = ((DraggableText) shape).getWidth();
                double height = ((DraggableText) shape).getHeight();
                double outlineThickness = shape.getStrokeWidth();
                JsonObject fillColorJson = makeJsonColorObject((Color) shape.getFill());
                JsonObject outlineColorJson = makeJsonColorObject(Color.BLACK);
                DraggableText draggableText = (DraggableText) shape;
                
                JsonObject textJSON = makeJsonTextObject(((DraggableText) shape).getText());
                JsonObject fontJSON = makeJsonFontObject(((DraggableText) shape).getFont());
                
                JsonObject shapeJSON = Json.createObjectBuilder()
                        .add(JSON_TYPE, ((DraggableText) shape).getShapeType())
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_FONT, fontJSON)
                        .add(JSON_TEXT, textJSON)
                        .add(JSON_FILL_COLOR, fillColorJson)
                        .add(JSON_OUTLINE_COLOR, outlineColorJson)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness)
                        .build();
                arrayBuilder.add(shapeJSON);
            } else if (shape instanceof Line) {
                double startx = ((Line) shape).getStartX();
                double starty = ((Line) shape).getStartY();
                double endx = ((Line) shape).getEndX();
                double endy = ((Line) shape).getEndY();
                
                double outlineThickness = shape.getStrokeWidth();
                JsonObject fillColorJson = makeJsonColorObject((Color) shape.getStroke());
                
                JsonObject shapeJSON = Json.createObjectBuilder()
                        .add(JSON_TYPE, "LINE")
                        .add(JSON_STARTX, startx)
                        .add(JSON_STARTY, starty)
                        .add(JSON_ENDX, endx)
                        .add(JSON_ENDY, endy)
                        .add(JSON_FILL_COLOR, fillColorJson)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness)
                        .build();
                arrayBuilder.add(shapeJSON);
            } else if (shape instanceof DraggableCircle) {
                double x = ((DraggableCircle) shape).getCenterX();
                double y = ((DraggableCircle) shape).getCenterY();
                double width = ((DraggableCircle) shape).getWidth();
                double height = ((DraggableCircle) shape).getHeight();
                JsonObject fillColorJson = makeJsonColorObject((Color)shape.getFill());
                JsonObject outlineColorJson = makeJsonColorObject(Color.BLACK);
                double outlineThickness = shape.getStrokeWidth();
                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, ((DraggableCircle) shape).getShapeType())
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_FILL_COLOR, fillColorJson)
                        .add(JSON_OUTLINE_COLOR, outlineColorJson)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
                
            } else if (shape instanceof DraggableImage) {
                double x = ((DraggableImage) shape).getX();
                double y = ((DraggableImage) shape).getY();
                double width = ((DraggableImage) shape).getWidth();
                double height = ((DraggableImage) shape).getHeight();
                try{
                JsonObject fillImageJson = makeJsonImgObject(((DraggableImage)(shape)).getFilePath());
                JsonObject outlineColorJson = makeJsonColorObject((Color) shape.getStroke());
                double outlineThickness = shape.getStrokeWidth();

                JsonObject shapeJson = Json.createObjectBuilder()
                        .add(JSON_TYPE, ((DraggableImage) shape).getShapeType())
                        .add(JSON_X, x)
                        .add(JSON_Y, y)
                        .add(JSON_WIDTH, width)
                        .add(JSON_HEIGHT, height)
                        .add(JSON_IMG_PATH, fillImageJson)
                        .add(JSON_OUTLINE_COLOR, outlineColorJson)
                        .add(JSON_OUTLINE_THICKNESS, outlineThickness).build();
                arrayBuilder.add(shapeJson);
                }catch (NullPointerException e)
                {
                    System.out.println("Null json");
                } 
            } else
                throw new NullPointerException("No shape type");
        }

        JsonArray shapesArray = arrayBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_BG_COLOR, bgColorJson)
                .add(JSON_SHAPES, shapesArray)
//                .add(JSON_LINES, )
//                .add(JSON_STATIONS)
                .build();
        
        
        
         // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
       
    }
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        mmmData dataManager = (mmmData) data;
        dataManager.resetData();
         // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // LOAD THE BACKGROUND COLOR
        Color bgColor = loadColor(json, JSON_BG_COLOR);
        dataManager.setBackgroundColor(bgColor);

        // AND NOW LOAD ALL THE SHAPES
        JsonArray jsonShapeArray = json.getJsonArray(JSON_SHAPES);
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject jsonShape = jsonShapeArray.getJsonObject(i);
            Shape shape = loadShape(jsonShape);
            dataManager.addShape(shape);
        }
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException{
        mmmData dataManager = (mmmData) data;

        JsonArrayBuilder lineArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder stationArrayBuilder = Json.createArrayBuilder();
        String filename = filePath.substring(filePath.lastIndexOf('\\')+1, filePath.length());
        
        LinkedList<DraggableLineWrapper> draglines = dataManager.getDraggableLines();
        LinkedList<DraggableCircle> dragstations = dataManager.getStations();
        
        for (DraggableLineWrapper node : draglines) {
            DraggableLineWrapper shape = node;
            
            String name = shape.getName();
            boolean circular = shape.getCircular();
            JsonObject lineColorJson = makeJsonColorObject((Color) shape.getColor());
                
                JsonObject lineJson = Json.createObjectBuilder()
                        .add(JSON_NAME, name)
                        .add(JSON_CIRCULAR, circular)
                        .add(JSON_COLOR, lineColorJson)
                        .add(JSON_STATION_NAMES, listStationNames(node))
                        .build();
                lineArrayBuilder.add(lineJson);
            
        }
        
        for (DraggableCircle node : dragstations) {
            DraggableCircle shape = node;
            String name = shape.getName();
            double x = shape.getCenterX();
            double y = shape.getCenterY();
            
            JsonObject stationJson = Json.createObjectBuilder()
                    .add(JSON_NAME, name)
                    .add(JSON_X, x)
                    .add(JSON_Y, y)
                    .build();
            stationArrayBuilder.add(stationJson);
        }
        
            JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_NAME, filename)
                .add(JSON_LINES, lineArrayBuilder)
                .add(JSON_STATIONS, stationArrayBuilder)
		.build();
            
             // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
    private double getDataAsDouble(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber) value;
        return number.bigDecimalValue().doubleValue();
    }
    
    private JsonArrayBuilder listStationNames(DraggableLineWrapper dl) {
        JsonArrayBuilder stationNames = Json.createArrayBuilder();
        for(int i = 0; i < dl.getStations().size(); i++)
        {
            stationNames.add(dl.getStations().get(i).getName());
        }
        return stationNames;
    }

    private Shape loadShape(JsonObject jsonShape) {
        // FIRST BUILD THE PROPER SHAPE TYPE

        String type = jsonShape.getString(JSON_TYPE);
        Shape shape;
        if (type.equals(LINE)) {
            shape = new Line();
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            
            double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setStroke(fillColor);
            shape.setStrokeWidth(outlineThickness);
            
            double startx = getDataAsDouble(jsonShape, JSON_STARTX);
            double starty = getDataAsDouble(jsonShape, JSON_STARTY);
            double endx = getDataAsDouble(jsonShape, JSON_ENDX);
            double endy = getDataAsDouble(jsonShape, JSON_ENDY);
            ((Line)shape).setStartX(startx);
            ((Line)shape).setStartY(starty);
            ((Line)shape).setEndX(endx);
            ((Line)shape).setEndY(endy);
            
        } else if (type.equals(CIRCLE)) {
            shape = new DraggableCircle();
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            
            
            ((DraggableCircle)shape).setRadius(width/2.0);
            shape.setFill(fillColor);
            shape.setStroke(outlineColor);
            shape.setStrokeWidth(outlineThickness);
            ((DraggableCircle)shape).setCenterX(x);
            ((DraggableCircle)shape).setCenterY(y);
            
        } else if (type.equals(TEXT)) {
            shape = new DraggableText();
            Color fillColor = loadColor(jsonShape, JSON_FILL_COLOR);
            Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            Font newFont = loadFont(jsonShape, JSON_FONT);
            String text = loadText(jsonShape, JSON_TEXT);
            shape.setFill(fillColor);
            shape.setStroke(outlineColor);
            shape.setStrokeWidth(outlineThickness);

            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);

            DraggableText draggableShape = (DraggableText) shape;
            draggableShape.setText(text);
            draggableShape.setFont(newFont);
            draggableShape.setLocationAndSize(x, y, width, height);

        } else {
            shape = new DraggableImage();
            Image fillImage = loadImage(jsonShape, JSON_IMG_PATH);
            Color outlineColor = loadColor(jsonShape, JSON_OUTLINE_COLOR);
            double outlineThickness = getDataAsDouble(jsonShape, JSON_OUTLINE_THICKNESS);
            shape.setFill(new ImagePattern(fillImage));
            shape.setStroke(outlineColor);
            shape.setStrokeWidth(outlineThickness);
        }

        // THEN LOAD ITS FILL AND OUTLINE PROPERTIES
        // AND THEN ITS DRAGGABLE PROPERTIES
        if (!type.equals(LINE) && !type.equals(CIRCLE)) {
            double x = getDataAsDouble(jsonShape, JSON_X);
            double y = getDataAsDouble(jsonShape, JSON_Y);
            double width = getDataAsDouble(jsonShape, JSON_WIDTH);
            double height = getDataAsDouble(jsonShape, JSON_HEIGHT);
            Draggable draggableShape = (Draggable) shape;
            draggableShape.setLocationAndSize(x, y, width, height);
        }
        // ALL DONE, RETURN IT
        return shape;
    }

    private Color loadColor(JsonObject json, String colorToGet) {
        JsonObject jsonColor = json.getJsonObject(colorToGet);
        double red = getDataAsDouble(jsonColor, JSON_RED);
        double green = getDataAsDouble(jsonColor, JSON_GREEN);
        double blue = getDataAsDouble(jsonColor, JSON_BLUE);
        double alpha = getDataAsDouble(jsonColor, JSON_ALPHA);
        Color loadedColor = new Color(red, green, blue, alpha);
        return loadedColor;
    }

    private Image loadImage(JsonObject json, String imgToGet) {
        JsonObject jsonImg = json.getJsonObject(imgToGet);
        Image newImage = new Image(jsonImg.getString(imgToGet));
        return newImage;
    }

    private Font loadFont(JsonObject json, String fontToGet) {
        JsonObject jsonFont = json.getJsonObject(fontToGet);
        Font font = new Font(jsonFont.getString(JSON_FNAME), getDataAsDouble(jsonFont, JSON_FSIZE));

        return font;
    }

    private String loadText(JsonObject json, String textToGet) {
        JsonObject jsonText = json.getJsonObject(textToGet);
        String s = jsonText.getString(JSON_TEXT);

        return s;
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    private JsonObject makeJsonColorObject(Color color) {
        JsonObject colorJson = Json.createObjectBuilder()
                .add(JSON_RED, color.getRed())
                .add(JSON_GREEN, color.getGreen())
                .add(JSON_BLUE, color.getBlue())
                .add(JSON_ALPHA, color.getOpacity()).build();
        return colorJson;
    }

    private JsonObject makeJsonFontObject(Font font) {
        JsonObject fontJson = Json.createObjectBuilder()
                .add(JSON_FNAME, font.getName())
                .add(JSON_FSIZE, font.getSize()).build();
        return fontJson;
    }

    private JsonObject makeJsonTextObject(String text) {
        JsonObject textJson = Json.createObjectBuilder()
                .add(JSON_TEXT, text).build();
        return textJson;
    }
    
    private JsonObject makeJsonImgObject(String filepath) {
        JsonObject paintJson = Json.createObjectBuilder()
                .add(JSON_IMG_PATH, filepath).build();
        return paintJson;
    }
    
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }
}
